#ifndef GFX_PS1_PRIVATE_H
#define GFX_PS1_PRIVATE_H

#include <gfx.h>

#ifdef __cplusplus
extern "C"
{
#endif

enum {DRAW_MODE = 0xE1};

enum
{
    SCREEN_W = 368,
    SCREEN_H = 240
};

void gfx_swapheap(void);
void gfx_initenvs(void);
void gfx_swapbuffers(void);

#ifdef __cplusplus
}
#endif

#endif /* GFX_PS1_PRIVATE_H */
