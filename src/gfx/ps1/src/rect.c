#include <gfx.h>
#include <ps1/gfx_private.h>
#include <psxgpu.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

static void common_init(struct rect *const r)
{
    memset(r, 0, sizeof *r);
    r->sznext.f.sz = (sizeof *r - sizeof r->sznext) / sizeof (uint32_t);
    r->common.f.cmd = DRAW_MODE;
}

void rect_init(struct rect *const r)
{
    common_init(r);
    r->cmd = 0x60;
}

void semitrans_rect_init(struct rect *const r)
{
    common_init(r);
    r->cmd = 0x62;
}
