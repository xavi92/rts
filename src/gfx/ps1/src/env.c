#include <gfx.h>
#include <gfx_private.h>
#include <ps1/gfx_private.h>
#include <psxgpu.h>
#include <stdbool.h>

static GsDispEnv dispenv;
enum {ENV_Y = SCREEN_H + 16};

static GsDrawEnv drawenv =
{
    .y = ENV_Y,
    .w = SCREEN_W,
    .h = SCREEN_H
};

int screen_w = SCREEN_W, screen_h = SCREEN_H;

void gfx_swapbuffers(void)
{
    const short y = drawenv.y;

    drawenv.y = dispenv.y;
    dispenv.y = y;
    GsSetDrawEnv(&drawenv);
    GsSetDispEnv(&dispenv);
}

void gfx_initenvs(void)
{
    GsSetDrawEnv(&drawenv);
    GsSetDispEnv(&dispenv);
}

int gfx_toggle_fullscreen(void)
{
    /* Not supported. */
    return -1;
}

bool gfx_toggle_fullscreen_available(void)
{
    return false;
}

bool gfx_fullscreen(void)
{
    return true;
}

bool gfx_inside_drawenv(const short x, const short y, const short w,
    const short h)
{
    return (x + w >= 0)
        && x < drawenv.w
        && (y + h >= 0)
		&& y < drawenv.h;
}
