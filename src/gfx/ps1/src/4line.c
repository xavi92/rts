#include <gfx.h>
#include <ps1/gfx_private.h>
#include <psxgpu.h>
#include <stdint.h>
#include <string.h>

void stp_4line_init(struct stp_4line *const l)
{
    memset(l, 0, sizeof *l);
    l->sznext.f.sz = (sizeof *l - sizeof l->sznext) / sizeof (uint32_t);
    l->common.f.cmd = DRAW_MODE;
    l->cmd = 0x5A;
    enum {TERMINATION_CODE = 0x55555555};
    l->end = TERMINATION_CODE;
}
