#include <gfx.h>
#include <ps1/gfx_private.h>
#include <gfx/port.h>
#include <stdint.h>
#include <string.h>

static void quad_init(struct quad *const q)
{
    memset(q, 0, sizeof *q);
    q->sznext.f.sz = (sizeof *q - sizeof q->sznext) / sizeof (uint32_t);
    q->cmd = 0x2d;
}

int quad_from_sprite(const struct sprite *const s, struct quad *const q)
{
    quad_init(q);
    q->tpage.mask = s->common.mask;
    q->clutid = s->clutid;
    q->u0 = q->u2 = s->u;
    q->v0 = q->v1 = s->v;
    q->u1 = q->u3 = s->u + s->w - 1;
    q->v2 = q->v3 = s->v + s->h - 1;
    return 0;
}
