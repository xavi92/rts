#include <gfx_private.h>
#include <ps1/gfx_private.h>
#include <psxgpu.h>

void gfx_deinit(void)
{
}

int gfx_init(void)
{
    GsInit();
    GsClearMem();
    GsSetVideoMode(SCREEN_W, SCREEN_H, VIDEO_MODE);
    gfx_initenvs();
    return 0;
}
