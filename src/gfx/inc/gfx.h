#ifndef GFX_H
#define GFX_H

#include <gfx/port.h>
#include <stdbool.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C"
{
#endif

int gfx_init(void);
int gfx_draw(void);
int gfx_toggle_fullscreen(void);
bool gfx_toggle_fullscreen_available(void);
bool gfx_fullscreen(void);
void sprite_sort(struct sprite *s);
int sprite_clone(const struct sprite *src, struct sprite *dst);
void rect_init(struct rect *r);
void semitrans_rect_init(struct rect *r);
void stp_4line_init(struct stp_4line *l);
void quad_sort(struct quad *q);
void rect_sort(struct rect *r);
void stp_4line_sort(struct stp_4line *l);
int sprite_from_fp(struct sprite *s, FILE *f);
int quad_from_sprite(const struct sprite *s, struct quad *q);
bool gfx_inside_drawenv(short x, short y, short w, short h);
void gfx_deinit(void);
void sprite_free(struct sprite *src);

#define sprite_get_or_ret(x, ret) common_get_or_ret(sprite, x, ret)
#define stp_4line_get_or_ret(x, ret) common_get_or_ret(stp_4line, x, ret)
#define rect_get_or_ret(x, ret) common_get_or_ret(rect, x, ret)
#define quad_get_or_ret(x, ret) common_get_or_ret(quad, x, ret)

extern int screen_w, screen_h;

#ifdef __cplusplus
}
#endif

#endif /* GFX_H */
