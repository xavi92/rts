#ifndef GFX_SDL_H
#define GFX_SDL_H

#include <SDL.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct sprite
{
    SDL_Surface *s, *s_x;
    short x, y, w, h;
    unsigned char u, v;
    bool transparent;
};

struct quad
{
    unsigned char r, g, b;
    short x0, x1, x2, x3;
    short y0, y1, y2, y3;
    unsigned char u0, u1, u2, u3;
    unsigned char v0, v1, v2, v3;
    short w, h;
    bool transparent;
    SDL_Surface *s, *s_x;
};

struct rect
{
    unsigned char r, g, b;
    short x, y, w, h;
    bool stp;
};

struct stp_4line
{
    short x, y;
    unsigned char r, g, b;

    struct stp_4line_vtx
    {
        unsigned char r, g, b;
        short x, y;
    } vertices[4];
};

#define common_get_or_ret(t, x, ret) \
    struct t x##__, *const x = &x##__

#ifdef __cplusplus
}
#endif

#endif /* GFX_SDL_H */
