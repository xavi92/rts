#ifndef GFX_SDL_12_PRIVATE_H
#define GFX_SDL_12_PRIVATE_H

#include <gfx.h>
#include <SDL.h>

#ifdef __cplusplus
extern "C"
{
#endif

enum
{
    SCREEN_W = 320,
    SCREEN_H = 240
};

int sprite_screen_resize_ev(struct sprite *s);
void gfx_register_sprite(struct sprite *s);
SDL_Surface *gfx_screen(void);

#ifdef __cplusplus
}
#endif

#endif /* GFX_SDL_12_PRIVATE_H */
