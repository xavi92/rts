#include <gfx.h>
#include <gfx/port.h>
#include <sdl-1.2/gfx_private.h>
#include <SDL.h>

void rect_sort(struct rect *const r)
{
    SDL_Rect rct =
    {
        .x = r->x,
        .y = r->y,
        .w = r->w,
        .h = r->h
    };

    SDL_Surface *const screen = gfx_screen();
    const Uint32 map = SDL_MapRGB(screen->format, r->r, r->g, r->b);

    if (SDL_FillRect(screen, &rct, map))
    {
        fprintf(stderr, "SDL_FillRect: %s\n", SDL_GetError());
        return;
    }
}

void rect_init(struct rect *const r)
{
    *r = (const struct rect){0};
}

void semitrans_rect_init(struct rect *const r)
{
    rect_init(r);
    r->stp = true;
}
