#ifndef NET_PRIVATE_H
#define NET_PRIVATE_H

#include <net.h>

struct net_socket
{
    enum net_domain d;
    struct net_socket_domain *s;
};

int net_init_ipv4(void);
void net_deinit_ipv4(void);
struct net_socket_domain *net_server_ipv4(const union net_server *srv);
struct net_socket_domain *net_connect_ipv4(const union net_connect *c);
int net_read_ipv4(struct net_socket_domain *h, void *buf, size_t n);
int net_write_ipv4(struct net_socket_domain *h, const void *buf, size_t n);
int net_close_ipv4(struct net_socket_domain *h);
int net_update_ipv4(struct net_socket_domain *h);

int net_init_serial(void);
void net_deinit_serial(void);
struct net_socket_domain *net_server_serial(const union net_server *srv);
struct net_socket_domain *net_connect_serial(const union net_connect *c);
int net_read_serial(struct net_socket_domain *h, void *buf, size_t n);
int net_write_serial(struct net_socket_domain *h, const void *buf, size_t n);
int net_close_serial(struct net_socket_domain *h);
int net_update_serial(struct net_socket_domain *h);

#endif /* NET_PRIVATE_H */
