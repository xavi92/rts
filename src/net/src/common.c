#include <net.h>

const char *net_domain_str(const enum net_domain d)
{
    static const char *const s[] =
    {
        [NET_DOMAIN_IPV4] = "UDP/IPv4",
        [NET_DOMAIN_SERIAL] = "Serial"
    };

    return s[d];
}
