#include <net.h>
#include <net_private.h>
#include <enet/enet.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

struct net_socket_domain
{
    ENetHost *host;
    ENetPeer *peers;
    struct net_connect_ev ev;
};

enum
{
    CHANNEL,
    MAX_CHANNELS
};

struct net_socket_domain *net_connect_ipv4(const union net_connect *const c)
{
    struct net_socket_domain *const s = calloc(1, sizeof *s);

    if (!s)
        goto failure;

    s->ev = c->common.ev;
    s->host = enet_host_create(NULL, 1, MAX_CHANNELS, 0, 0);

    if (!s->host)
    {
        fprintf(stderr, "%s: enet_host_create failed\n", __func__);
        goto failure;
    }

    ENetAddress addr;

    if (enet_address_set_host(&addr, c->ipv4.addr))
    {
        fprintf(stderr, "%s: enet_address_set_host failed\n", __func__);
        goto failure;
    }

    addr.port = c->ipv4.port;

    if (!(s->peers = enet_host_connect(s->host, &addr, MAX_CHANNELS, 0)))
    {
        fprintf(stderr, "%s: enet_host_connect failed\n", __func__);
        goto failure;
    }

    return s;

failure:

    if (s && s->host)
        enet_host_destroy(s->host);

    free(s);
    return NULL;
}

int net_read_ipv4(struct net_socket_domain *const s, void *const buf,
    const size_t n)
{
    return -1;
}

int net_write_ipv4(struct net_socket_domain *const s, const void *const buf,
    const size_t n)
{
    return -1;
}

int net_close_ipv4(struct net_socket_domain *const s)
{
    if (s && s->host)
        enet_host_destroy(s->host);

    free(s);
    return 0;
}

int net_update_ipv4(struct net_socket_domain *const s)
{
    int res;
    ENetEvent ev;

    while ((res = enet_host_service(s->host, &ev, 0)) > 0)
    {
        switch (ev.type)
        {
            case ENET_EVENT_TYPE_CONNECT:
                if (s->ev.connected)
                    s->ev.connected(s->ev.arg);

                break;

            case ENET_EVENT_TYPE_DISCONNECT:
                if (s->ev.disconnected)
                    s->ev.disconnected(s->ev.arg);

                break;

            case ENET_EVENT_TYPE_RECEIVE:
                break;

            case ENET_EVENT_TYPE_NONE:
                break;
        }
    }

    if (res < 0)
    {
        fprintf(stderr, "%s: enet_host_service failed\n", __func__);
        return res;
    }

    return 0;
}

struct net_socket_domain *net_server_ipv4(const union net_server *const srv)
{
    struct net_socket_domain *const s = calloc(1, sizeof *s);

    if (!s)
        goto failure;

    const ENetAddress addr =
    {
        .port = srv->ipv4.port
    };

    s->host = enet_host_create(&addr, srv->common.max_players,
        MAX_CHANNELS, 0, 0);

    if (!s->host)
    {
        fprintf(stderr, "%s: enet_host_create failed\n", __func__);
        goto failure;
    }

    return s;

failure:
    free(s);
    return NULL;
}

int net_init_ipv4(void)
{
    return enet_initialize();
}

void net_deinit_ipv4(void)
{
    enet_deinitialize();
}
