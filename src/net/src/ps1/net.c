#include <net.h>
#include <net/serial.h>
#include <errno.h>
#include <stdbool.h>
#include <stddef.h>

int net_read(struct net_socket *const h, void *const buf, const size_t n)
{
    return -1;
}

int net_write(struct net_socket *const h, const void *const buf, const size_t n)
{
    return -1;
}

int net_close(struct net_socket *const h)
{
    return -1;
}

int net_update(struct net_socket *const h)
{
    return -1;
}

struct net_socket *net_connect(const union net_connect *const c)
{
    return NULL;
}

struct net_socket *net_server(const union net_server *const c)
{
    return NULL;
}

int net_init(void)
{
    return 0;
}

void net_deinit(void)
{
}

bool net_available(const enum net_domain d)
{
    return d == NET_DOMAIN_SERIAL;
}

const char *const *net_serial_devices(size_t *const n)
{
    static const char *const dev[] =
    {
        "PS1 port"
    };

    *n = sizeof dev / sizeof *dev;
    return dev;
}
