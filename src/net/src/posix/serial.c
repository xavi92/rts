#include <net.h>
#include <net/serial.h>
#include <net_private.h>
#include <stddef.h>

int net_read_serial(struct net_socket_domain *const h, void *const buf,
    const size_t n)
{
    return -1;
}

int net_write_serial(struct net_socket_domain *const h, const void *const buf,
    const size_t n)
{
    return -1;
}

int net_close_serial(struct net_socket_domain *const h)
{
    return -1;
}

int net_update_serial(struct net_socket_domain *const h)
{
    return -1;
}

struct net_socket_domain *net_connect_serial(const union net_connect *const srv)
{
    return NULL;
}

struct net_socket_domain *net_server_serial(const union net_server *const srv)
{
    return NULL;
}

int net_init_serial(void)
{
    return 0;
}

void net_deinit_serial(void)
{
}

const char *const *net_serial_devices(size_t *const n)
{
    return NULL;
}
