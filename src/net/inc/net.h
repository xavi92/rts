#ifndef NET_H
#define NET_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

enum net_domain
{
    NET_DOMAIN_IPV4,
    NET_DOMAIN_SERIAL
};

union net_connect
{
    struct net_connect_common
    {
        enum net_domain domain;

        struct net_connect_ev
        {
            void (*connected)(void *arg);
            void (*disconnected)(void *arg);
            void *arg;
        } ev;
    } common;

    struct net_connect_ipv4
    {
        struct net_connect_common common;
        const char *addr;
        uint16_t port;
    } ipv4;

    struct net_connect_serial
    {
        struct net_connect_common common;
        const char *dev;
        unsigned long baud;

        enum
        {
            NET_PARITY_NONE,
            NET_PARITY_ODD,
            NET_PARITY_EVEN
        } parity;
    } serial;
};

union net_server
{
    struct net_server_common
    {
        enum net_domain domain;
        unsigned max_players;
    } common;

    struct net_server_ipv4
    {
        struct net_server_common common;
        uint16_t port;
    } ipv4;

    struct net_server_serial
    {
        struct net_server_common common;
    } serial;
};

struct net_socket;

int net_init(void);
void net_deinit(void);
int net_update(struct net_socket *s);
bool net_available(enum net_domain d);
struct net_socket *net_server(const union net_server *srv);
struct net_socket *net_connect(const union net_connect *c);
int net_read(struct net_socket *s, void *buf, size_t n);
int net_write(struct net_socket *s, const void *buf, size_t n);
int net_close(struct net_socket *s);
const char *net_domain_str(enum net_domain d);

#ifdef __cplusplus
}
#endif

#endif /* NET_H */
