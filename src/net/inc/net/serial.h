#ifndef NET_SERIAL_H
#define NET_SERIAL_H

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

const char *const *net_serial_devices(size_t *n);

#ifdef __cplusplus
}
#endif

#endif /* NET_SERIAL_H */
