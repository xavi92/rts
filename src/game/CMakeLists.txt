add_library(game "src/game.c" "src/res.c")
target_include_directories(game PUBLIC "inc" PRIVATE "privinc")
target_link_libraries(game PRIVATE
    building
    container
    font
    gfx
    gui
    instance
    pad
    player
    resource
    system
    terrain
    unit)
