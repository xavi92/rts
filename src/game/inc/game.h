#ifndef GAME_H
#define GAME_H

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct game_cfg
{
};

int game_resinit(void);
int game(const struct game_cfg *cfg);
void game_free(void);

#ifdef __cplusplus
}
#endif

#endif /* GAME_H */
