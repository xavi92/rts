#include <game_private.h>
#include <building.h>
#include <container.h>
#include <font.h>
#include <gfx.h>
#include <gui/bar.h>
#include <gui/button.h>
#include <gui/line_edit.h>
#include <gui/rounded_rect.h>
#include <gui/checkbox.h>
#include <resource.h>
#include <terrain.h>
#include <unit.h>
#include <stdbool.h>

static const struct container c[] =
{
    {
        .path = "barracks",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &building_sprites[BUILDING_TYPE_BARRACKS]
        }
    },

    {
        .path = "worker_n",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &unit_sprites[UNIT_SPRITE_N]
        }
    },

    {
        .path = "worker_ne",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &unit_sprites[UNIT_SPRITE_NE]
        }
    },

    {
        .path = "worker_e",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &unit_sprites[UNIT_SPRITE_E]
        }
    },

    {
        .path = "worker_se",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &unit_sprites[UNIT_SPRITE_SE]
        }
    },

    {
        .path = "worker_s",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &unit_sprites[UNIT_SPRITE_S]
        }
    },

    {
        .path = "grass",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &grass_sprite
        }
    },

    {
        .path = "cursor",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &cursor_sprite
        }
    },

    {
        .path = "gui_bar_left",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_bar_sprites[GUI_BAR_LEFT]
        }
    },

    {
        .path = "gui_bar_mid",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_bar_sprites[GUI_BAR_MID]
        }
    },

    {
        .path = "gui_bar_right",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_bar_sprites[GUI_BAR_RIGHT]
        }
    },

    {
        .path = "sel_up_left",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_rounded_rect_sprites[GUI_ROUNDED_RECT_UP_LEFT]
        }
    },

    {
        .path = "sel_up_right",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_rounded_rect_sprites[GUI_ROUNDED_RECT_UP_RIGHT]
        }
    },

    {
        .path = "sel_down_left",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_rounded_rect_sprites[GUI_ROUNDED_RECT_DOWN_LEFT]
        }
    },

    {
        .path = "sel_down_right",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_rounded_rect_sprites[GUI_ROUNDED_RECT_DOWN_RIGHT]
        }
    },

    {
        .path = "sel_mid",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_rounded_rect_sprites[GUI_ROUNDED_RECT_MID]
        }
    },

    {
        .path = "sel_mid_v",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_rounded_rect_sprites[GUI_ROUNDED_RECT_MID_VERT]
        }
    },

    {
        .path = "font",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &font_sprite
        }
    },

    {
        .path = "gold_mine",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &resource_sprites[RESOURCE_TYPE_GOLD]
        }
    },

    {
        .path = "tree",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &resource_sprites[RESOURCE_TYPE_WOOD]
        }
    },

    {
        .path = "acknowledge_01",
        .type = CONTAINER_TYPE_SOUND,
        .data =
        {
            .sound = &unit_sounds[UNIT_SOUND_MOVE]
        }
    },

    {
        .path = "acknowledge_02",
        .type = CONTAINER_TYPE_SOUND,
        .data =
        {
            .sound = &unit_sounds[UNIT_SOUND_MOVE_2]
        }
    },

    {
        .path = "selected_01",
        .type = CONTAINER_TYPE_SOUND,
        .data =
        {
            .sound = &unit_sounds[UNIT_SOUND_SELECTED]
        }
    },

    {
        .path = "btn_left",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_button_sprites[GUI_BUTTON_LEFT]
        }
    },

    {
        .path = "btn_mid",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_button_sprites[GUI_BUTTON_MID]
        }
    },

    {
        .path = "btn_right",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_button_sprites[GUI_BUTTON_RIGHT]
        }
    },

    {
        .path = "line_edit_left",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_line_edit_sprites[GUI_LINE_EDIT_LEFT]
        }
    },

    {
        .path = "line_edit_mid",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_line_edit_sprites[GUI_LINE_EDIT_MID]
        }
    },

    {
        .path = "line_edit_right",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_line_edit_sprites[GUI_LINE_EDIT_RIGHT]
        }
    },

    {
        .path = "checkbox",
        .type = CONTAINER_TYPE_SPRITE,
        .data =
        {
            .sprite = &gui_checkbox_sprite
        }
    }
};

static bool init;

void game_free(void)
{
    if (init)
    {
        container_free(c, sizeof c / sizeof *c);
        init = false;
    }
}

int game_resinit(void)
{
    if (!init)
    {
        if (container_load("rts.cnt", c, sizeof c / sizeof *c))
        {
            perror("container_load");
            return -1;
        }

        init = true;
    }

    return 0;
}
