#include <game.h>
#include <game_private.h>
#include <gfx.h>
#include <human_player.h>
#include <player.h>
#include <resource.h>
#include <system.h>
#include <terrain.h>
#include <stddef.h>

int game(const struct game_cfg *const cfg)
{
    int ret = -1;
    enum {HUMAN_PLAYERS = 1, MAP_RESOURCES = 3};
    struct human_player humans[HUMAN_PLAYERS];
    struct terrain_map map;

    terrain_init(&map);
    building_set_alive_cb(terrain_block_update, &map);

    for (size_t i = 0; i < sizeof humans / sizeof *humans; i++)
    {
        const struct human_player_cfg cfg =
        {
            .sel_periph = PERIPHERAL_TYPE_KEYBOARD_MOUSE,
            .padn = i,
            .dim =
            {
                .w = MAP_W,
                .h = MAP_H,
            },

            .pl =
            {
                .team = PLAYER_COLOR_BLUE,
                .x = 80,
                .y = 40
            }
        };

        if (human_player_init(&cfg, &humans[i]))
            goto end;
    }

    struct resource res[MAP_RESOURCES] = {0};

    resource_set_alive_cb(terrain_block_update, &map);

    if (resource_create(&(const struct resource_cfg)
        {
            .type = RESOURCE_TYPE_GOLD,
            .x = 50,
            .y = 200
        }, res, sizeof res / sizeof *res)
        || resource_create(&(const struct resource_cfg)
        {
            .type = RESOURCE_TYPE_WOOD,
            .x = 180,
            .y = 200
        }, res, sizeof res / sizeof *res)
        || resource_create(&(const struct resource_cfg)
        {
            .type = RESOURCE_TYPE_WOOD,
            .x = 240,
            .y = 200
        }, res, sizeof res / sizeof *res))
        goto end;

    bool exit = false;

    while (!exit)
    {
        system_loop();

        for (size_t i = 0; i < sizeof humans / sizeof *humans; i++)
        {
            const struct human_player *const h = &humans[i];

            if (h->pl.alive)
            {
                struct player_others o =
                {
                    .res = res,
                    .n_res = sizeof res / sizeof *res
                };

                human_player_update(&humans[i], &o);
                exit |= humans[i].periph.common.exit;
                terrain_update(&map);

                if (terrain_render(&map, &h->cam)
                    || human_player_render(h, &o))
                    goto end;

                /* TODO: render AI players. */
            }
        }

        instance_cyclic();

        if (gfx_draw())
            goto end;
    }

    ret = 0;

end:
    return ret;
}
