#ifndef FONT_H
#define FONT_H

#ifdef __cplusplus
extern "C"
{
#endif

enum font
{
    FONT
};

int font_puts(enum font, short x, short y, const char *s);
int font_dim(enum font f, const char *str, short *x, short *y);

extern struct sprite font_sprite;

#ifdef __cplusplus
}
#endif

#endif /* FONT_H */
