add_library(font "src/font.c")
target_include_directories(font PUBLIC "inc")
target_link_libraries(font PUBLIC container PRIVATE gfx)
