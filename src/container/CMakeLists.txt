set(inc "inc")

if(PS1_BUILD)
    set(inc ${inc} "ps1/inc")
elseif(SDL1_2_BUILD)
    set(inc ${inc} "sdl-1.2/inc")
endif()

add_library(container "src/container.c")
target_include_directories(container PUBLIC ${inc})
target_link_libraries(container PUBLIC gfx sfx)
