#ifndef CONTAINER_H
#define CONTAINER_H

#include <container/port.h>
#include <gfx.h>
#include <sfx.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct container
{
    const char *path;

    enum
    {
        CONTAINER_TYPE_SPRITE,
        CONTAINER_TYPE_SOUND
    } type;

    union
    {
        struct sprite *sprite;
        struct sound *sound;
    } data;

    struct container_rt
    {
        struct container *c;
    } *rt;
};

int container_load_ex(const char *path, const struct container *list, size_t n);
void container_free(const struct container *list, size_t n);

#ifdef __cplusplus
}
#endif

#endif /* CONTAINER_H */
