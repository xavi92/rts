#include <header.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int header_load_bool(FILE *const f, const char *const key, bool *const value)
{
    for (size_t i = 0; i < strlen(key); i++)
    {
        const int ci = fgetc(f);

        if (ferror(f) || ci != key[i])
        {
            fprintf(stderr, "%s: failed to read property %s\n",
                __func__, key);
            return -1;
        }
    }

    {
        const int ci = fgetc(f);

        if (ferror(f) || ci != '=')
        {
            fprintf(stderr, "%s: expected '=' on property %s\n",
                __func__, key);
            return -1;
        }
    }

    {
        static const char exp[] = "1";
        char str[sizeof exp];

        for (char *c = str; c - str < sizeof str; c++)
        {
            const int ci = fgetc(f);

            if (ferror(f) || ci == EOF)
            {
                fprintf(stderr, "%s: could not read value for property %s\n",
                    __func__, key);
                return -1;
            }

            *c = ci;
        }

        errno = 0;
        const unsigned long out = strtoul(str, NULL, 10);

        if (errno || (out != 0 && out != 1))
        {
            fprintf(stderr, "%s: unexpected value for property %s: %lu\n",
                __func__, key, out);
            return -1;
        }

        *value = out;
    }

    return 0;
}
