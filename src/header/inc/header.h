#ifndef HEADER_H
#define HEADER_H

#include <stdbool.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C"
{
#endif

int header_load_bool(FILE *f, const char *key, bool *value);

#ifdef __cplusplus
}
#endif

#endif /* HEADER_H */
