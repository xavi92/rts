#ifndef GUI_CONTAINER_H
#define GUI_CONTAINER_H

#include <gui.h>
#include <util.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct gui_container
{
    struct gui_common common;
    short w, h, spacing;

    enum
    {
        GUI_CONTAINER_MODE_H,
        GUI_CONTAINER_MODE_V
    } mode;
};

UTIL_STATIC_ASSERT(!offsetof(struct gui_container, common),
    "unexpected offset for struct gui_container");

void gui_container_init(struct gui_container *c);

#ifdef __cplusplus
}
#endif

#endif /* GUI_CONTAINER_H */
