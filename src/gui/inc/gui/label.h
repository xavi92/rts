#ifndef GUI_LABEL_H
#define GUI_LABEL_H

#include <gui.h>
#include <font.h>
#include <util.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct gui_label
{
    struct gui_common common;
    enum font font;
    const char *text;
};

UTIL_STATIC_ASSERT(!offsetof(struct gui_label, common),
    "unexpected offset for struct gui_label");

void gui_label_init(struct gui_label *l);

#ifdef __cplusplus
}
#endif

#endif /* GUI_LABEL_H */
