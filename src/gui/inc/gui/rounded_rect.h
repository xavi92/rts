#ifndef GUI_ROUNDED_RECT_H
#define GUI_ROUNDED_RECT_H

#include <gui.h>
#include <gfx.h>
#include <util.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct gui_rounded_rect
{
    struct gui_common common;
    unsigned short w, h;
};

UTIL_STATIC_ASSERT(!offsetof(struct gui_rounded_rect, common),
    "unexpected offset for struct gui_rounded_rect");

void gui_rounded_rect_init(struct gui_rounded_rect *r);

enum
{
    GUI_ROUNDED_RECT_UP_LEFT,
    GUI_ROUNDED_RECT_UP_RIGHT,
    GUI_ROUNDED_RECT_MID_VERT,
    GUI_ROUNDED_RECT_DOWN_LEFT,
    GUI_ROUNDED_RECT_DOWN_RIGHT,
    GUI_ROUNDED_RECT_MID,

    MAX_GUI_ROUNDED_RECT_SPRITES
};

extern struct sprite gui_rounded_rect_sprites[MAX_GUI_ROUNDED_RECT_SPRITES];

#ifdef __cplusplus
}
#endif

#endif /* GUI_ROUNDED_RECT_H */
