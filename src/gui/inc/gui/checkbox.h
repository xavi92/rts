#ifndef GUI_CHECKBOX_H
#define GUI_CHECKBOX_H

#include <gui.h>
#include <gfx.h>
#include <util.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct gui_checkbox
{
    struct gui_common common;
    bool active;
    void *arg;
    void (*on_pressed)(bool active, void *arg);
};

void gui_checkbox_init(struct gui_checkbox *c);

extern struct sprite gui_checkbox_sprite;

UTIL_STATIC_ASSERT(!offsetof(struct gui_checkbox, common),
    "unexpected offset for struct gui_checkbox");

#ifdef __cplusplus
}
#endif

#endif /* GUI_CHECKBOX_H */
