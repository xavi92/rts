#ifndef GUI_PROGRESS_BAR_H
#define GUI_PROGRESS_BAR_H

#include <gui.h>
#include <util.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

enum {GUI_PROGRESS_BAR_MAX = 255};

struct gui_progress_bar
{
    struct gui_common common;
    short w, h;
    bool stp;
    unsigned char progress;
    struct
    {
        unsigned char r, g, b;
    } fg, bg;
};

UTIL_STATIC_ASSERT(!offsetof(struct gui_progress_bar, common),
    "unexpected offset for struct gui_progress_bar");

void gui_progress_bar_init(struct gui_progress_bar *pb);

#ifdef __cplusplus
}
#endif

#endif /* GUI_PROGRESS_BAR_H */
