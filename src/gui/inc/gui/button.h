#ifndef GUI_BUTTON_H
#define GUI_BUTTON_H

#include <gfx.h>
#include <gui.h>
#include <gui/label.h>
#include <util.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct gui_button
{
    struct gui_common common;

    enum gui_button_type
    {
        GUI_BUTTON_TYPE_1,
        GUI_BUTTON_TYPE_SPRITE
    } type;

    union
    {
        struct
        {
            short w;
            struct gui_label label;
        } type1;

        struct
        {
            const struct sprite *s;
        } sprite;
    } u;

    void *arg;
    void (*on_pressed)(void *);
};

UTIL_STATIC_ASSERT(!offsetof(struct gui_button, common),
    "unexpected offset for struct gui_button");

void gui_button_init(struct gui_button *b, enum gui_button_type t);

enum
{
    GUI_BUTTON_LEFT,
    GUI_BUTTON_MID,
    GUI_BUTTON_RIGHT,

    MAX_GUI_BUTTON_SPRITES
};

extern struct sprite gui_button_sprites[MAX_GUI_BUTTON_SPRITES];

#ifdef __cplusplus
}
#endif

#endif /* GUI_BUTTON_H */
