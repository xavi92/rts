#ifndef GUI_BAR_H
#define GUI_BAR_H

#include <gui.h>
#include <gfx.h>
#include <util.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct gui_bar
{
    struct gui_common common;
    short w;
};

void gui_bar_init(struct gui_bar *b);

enum
{
    GUI_BAR_LEFT,
    GUI_BAR_MID,
    GUI_BAR_RIGHT,

    MAX_GUI_BAR_SPRITES
};

UTIL_STATIC_ASSERT(!offsetof(struct gui_bar, common),
    "unexpected offset for struct gui_bar");

extern struct sprite gui_bar_sprites[MAX_GUI_BAR_SPRITES];

#ifdef __cplusplus
}
#endif

#endif /* GUI_BAR_H */
