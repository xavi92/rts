#ifndef GUI_LINE_EDIT_H
#define GUI_LINE_EDIT_H

#include <gui.h>
#include <gui/label.h>
#include <gfx.h>
#include <util.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct gui_line_edit
{
    struct gui_common common;
    struct gui_label label;
    short w;
    bool focus, blink;
    unsigned blt;
    char *text;
    size_t i, sz;
};

void gui_line_edit_init(struct gui_line_edit *l, char *buf, size_t sz);

UTIL_STATIC_ASSERT(!offsetof(struct gui_line_edit, common),
    "unexpected offset for struct gui_line_edit");

enum
{
    GUI_LINE_EDIT_LEFT,
    GUI_LINE_EDIT_MID,
    GUI_LINE_EDIT_RIGHT,

    MAX_GUI_LINE_EDIT_SPRITES
};

extern struct sprite gui_line_edit_sprites[MAX_GUI_LINE_EDIT_SPRITES];

#ifdef __cplusplus
}
#endif

#endif /* GUI_LINE_EDIT_H */
