#ifndef GUI_H
#define GUI_H

#include <camera.h>
#include <gfx.h>
#include <input.h>
#include <peripheral.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct gui_common
{
    const struct gui_common_cb
    {
        void (*add_child)(struct gui_common *parent, struct gui_common *child);
        int (*update)(struct gui_common *, const union peripheral *,
            const struct camera *, struct input *);
        int (*render)(const struct gui_common *);
        void (*get_dim)(const struct gui_common *, short *w, short *h);
        void (*deinit)(struct gui_common *, struct input *);
    } *cb;

    short x, y, xoff, yoff;
    bool hidden, hcentered, vcentered;
    struct gui_common *parent, *child, *sibling;
};

void gui_add_child(struct gui_common *parent, struct gui_common *child);
void gui_add_sibling(struct gui_common *g, struct gui_common *sibling);
int gui_update(struct gui_common *g, const union peripheral *p,
    const struct camera *c, struct input *in);
int gui_render(const struct gui_common *g);
void gui_deinit(struct gui_common *g, struct input *in);

#ifdef __cplusplus
}
#endif

#endif /* GUI_H */
