#ifndef GUI_BUTTON_PRIVATE_H
#define GUI_BUTTON_PRIVATE_H

#include <gui/button.h>

#ifdef __cplusplus
extern "C"
{
#endif

void gui_button_init_type1(struct gui_button *b);
void gui_button_init_sprite(struct gui_button *b);
int gui_button_render_type1(const struct gui_button *b);
int gui_button_render_sprite(const struct gui_button *b);
void gui_button_get_dim_type1(const struct gui_button *b, short *w, short *h);
void gui_button_get_dim_sprite(const struct gui_button *b, short *w, short *h);

#ifdef __cplusplus
}
#endif

#endif /* GUI_BUTTON_PRIVATE_H */
