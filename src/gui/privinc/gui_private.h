#ifndef GUI_PRIVATE_H
#define GUI_PRIVATE_H

#include <gui.h>
#include <input.h>
#include <peripheral.h>
#include <camera.h>

#ifdef __cplusplus
extern "C"
{
#endif

void gui_coords(const struct gui_common *g, short *x, short *y);
bool gui_pressed(const struct gui_common *g, const struct input *in,
    const union peripheral *p, const struct camera *cam, short w, short h);
bool gui_released(const struct gui_common *g, const union peripheral *p,
    const struct camera *cam, short w, short h);

#ifdef __cplusplus
}
#endif

#endif /* GUI_PRIVATE_H */
