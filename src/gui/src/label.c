#include <gui/label.h>
#include <gui.h>
#include <gui_private.h>
#include <font.h>

static int render(const struct gui_common *const g)
{
    const struct gui_label *const l = (const struct gui_label *)g;
    short x, y;

    gui_coords(g, &x, &y);

    return l->text ? font_puts(l->font, x, y, l->text) : -1;
}

static void get_dim(const struct gui_common *const g,
    short *const w, short *const h)
{
    const struct gui_label *const l = (const struct gui_label *)g;

    font_dim(l->font, l->text, w, h);
}

void gui_label_init(struct gui_label *const l)
{
    static const struct gui_common_cb cb =
    {
        .get_dim = get_dim,
        .render = render
    };

    *l = (const struct gui_label)
    {
        .common =
        {
            .cb = &cb
        },

        .text = ""
    };
}
