#ifndef MOUSE_H
#define MOUSE_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

enum mouse_button
{
    MOUSE_BUTTON_LEFT,
    MOUSE_BUTTON_RIGHT
};

struct mouse
{
    short x, y, dx, dy;
    int mask, oldmask;
};

void mouse_init(struct mouse *m);
void mouse_update(struct mouse *m);
bool mouse_pressed(const struct mouse *m, enum mouse_button b);
bool mouse_justpressed(const struct mouse *m, enum mouse_button b);
bool mouse_justreleased(const struct mouse *m, enum mouse_button b);

#ifdef __cplusplus
}
#endif

#endif /* MOUSE_H */
