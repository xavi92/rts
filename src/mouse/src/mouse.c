#include <mouse.h>
#include <stdbool.h>

bool mouse_pressed(const struct mouse *const m, const enum mouse_button b)
{
    return m->mask & (1 << b);
}

bool mouse_justpressed(const struct mouse *const m,
    const enum mouse_button b)
{
    return m->mask & (1 << b) && !(m->oldmask & (1 << b));
}

bool mouse_justreleased(const struct mouse *const m,
    const enum mouse_button b)
{
    return !(m->mask & (1 << b)) && m->oldmask & (1 << b);
}
