#ifndef CAMERA_PRIVATE_H
#define CAMERA_PRIVATE_H

#include <camera.h>
#include <input.h>
#include <mouse.h>
#include <pad.h>

#ifdef __cplusplus
extern "C"
{
#endif

enum
{
    CAMERA_CURSOR_WIDTH = 20,
    CAMERA_CURSOR_HEIGHT = 20
};

void camera_update_pos(struct camera *cam);
void camera_update_pad(struct camera *cam, const struct pad *p,
    const struct input *in);
void camera_update_mouse(struct camera *cam, const struct mouse *m);
void camera_update_touch(struct camera *cam, const struct mouse *m,
    const struct input *in);

#ifdef __cplusplus
}
#endif

#endif /* CAMERA_PRIVATE_H */
