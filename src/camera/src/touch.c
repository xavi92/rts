#include <camera.h>
#include <mouse.h>
#include <camera_private.h>
#include <gfx.h>

static void cursor_update(struct cursor *const c, const struct mouse *const m)
{
    if (c->screen.last_w != screen_w
        || c->screen.last_h != screen_h)
        cursor_init(c);

    c->x = m->x;
    c->y = m->y;
}

static void update_speed(struct camera *const cam, const struct mouse *const m,
    const struct input *const in)
{
    int *const sx = &cam->x_speed, *const sy = &cam->y_speed;

    if (input_mouse_pressed(in, m, MOUSE_BUTTON_LEFT))
    {
        *sx = m->dx;
        *sy = m->dy;

        cam->pan = *sx || *sy;
    }
    else if (*sx || *sy)
    {
        const int qx = *sx / 4;

        if (qx)
            *sx -= qx;
        else
            *sx = 0;

        const int qy = *sy / 4;

        if (qy)
            *sy -= qy;
        else
            *sy = 0;
    }
}

void camera_update_touch(struct camera *const cam, const struct mouse *const m,
    const struct input *const in)
{
    cursor_update(&cam->cursor, m);
    update_speed(cam, m, in);
    camera_update_pos(cam);
}
