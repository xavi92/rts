#ifndef CAMERA_H
#define CAMERA_H

#include <input.h>
#include <peripheral.h>
#include <util.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct camera
{
    struct camera_dim
    {
        long w, h;
    } dim;

    int x, y, x_speed, y_speed;
    unsigned int xt, yt;
    bool pan;

    struct cursor
    {
        unsigned int x, y, x_init, y_init;
        enum
        {
            CURSOR_STATE_IDLE,
            CURSOR_STATE_PRESSED
        } state;
        struct
        {
            int last_w, last_h;
        } screen;

        struct cursor_pos_rt
        {
            const struct cursor_pos
            {
                unsigned x, y;
            } *list;

            size_t i, n;
        } rt;
    } cursor;
};

extern struct sprite cursor_sprite;

void camera_update(struct camera *cam, const union peripheral *p, const struct input *in);
bool camera_translate(const struct camera *cam, const struct util_rect *dim, short *x, short *y);
void cursor_init(struct cursor *c);
bool cursor_collision(const struct camera *cam, const struct util_rect *d);
void cursor_pos(const struct camera *cam, unsigned long *x, unsigned long *y);
void cursor_set_pos_list(struct cursor *c, const struct cursor_pos *pos, size_t n);
int cursor_render(const struct cursor *c);

#ifdef __cplusplus
}
#endif

#endif /* CAMERA_H */
