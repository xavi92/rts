#ifndef MENU_H
#define MENU_H

#ifdef __cplusplus
extern "C"
{
#endif

int menu(void);

#ifdef __cplusplus
}
#endif

#endif /* MENU_H */
