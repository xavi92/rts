#ifndef MENU_PRIVATE_H
#define MENU_PRIVATE_H

#include <camera.h>
#include <input.h>
#include <peripheral.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct menu_common
{
    struct camera cam;
    union peripheral p;
    struct input in;
};

int menu_update(struct menu_common *c,
    int (*update)(struct menu_common *, void *),
    int (*render)(const struct menu_common *, void *),
    void *arg);
int menu_main(struct menu_common *c);
int menu_hostjoin(struct menu_common *c, bool *back);
int menu_join(struct menu_common *c);
int menu_gamecfg(struct menu_common *c);
void menu_on_pressed(void *arg);

#ifdef __cplusplus
}
#endif

#endif /* MENU_PRIVATE_H */
