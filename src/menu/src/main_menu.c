#include <menu.h>
#include <menu_private.h>
#include <gui.h>
#include <gui/button.h>
#include <gui/container.h>
#include <gui/rounded_rect.h>
#include <system.h>
#include <stdbool.h>

struct main_menu
{
    bool start, exit;
    struct gui_button play, exit_btn;
    struct gui_container cnt;
};

static int update(struct menu_common *const c, void *const arg)
{
    struct main_menu *const m = arg;

    if (gui_update(&m->play.common, &c->p, &c->cam, &c->in))
        return -1;

    return 0;
}

static int render(const struct menu_common *const c, void *const arg)
{
    const struct main_menu *const m = arg;

    if (gui_render(&m->cnt.common))
        return -1;

    return 0;
}

int menu_main(struct menu_common *const c)
{
    bool back;

    do
    {
        struct main_menu m = {0};

        back = false;

        gui_container_init(&m.cnt);
        m.cnt.mode = GUI_CONTAINER_MODE_V;
        m.cnt.common.hcentered = true;
        m.cnt.common.vcentered = true;
        m.cnt.spacing = 4;

        gui_button_init(&m.play, GUI_BUTTON_TYPE_1);
        m.play.on_pressed = menu_on_pressed;
        m.play.arg = &m.start;
        m.play.u.type1.w = 140;
        m.play.common.hcentered = true;
        m.play.u.type1.label.text = "Play";
        gui_add_child(&m.cnt.common, &m.play.common);

        if (system_can_exit())
        {
            gui_button_init(&m.exit_btn, GUI_BUTTON_TYPE_1);
            m.exit_btn.arg = &m.exit;
            m.exit_btn.u.type1.w = 140;
            m.exit_btn.common.hcentered = true;
            m.exit_btn.u.type1.label.text = "Exit";
            m.exit_btn.on_pressed = menu_on_pressed;
            gui_add_child(&m.cnt.common, &m.exit_btn.common);
        }

        while (!m.start)
        {
            if (menu_update(c, update, render, &m))
                return -1;

            if (m.exit)
                return 0;
        }

        if (menu_hostjoin(c, &back))
            return -1;

    } while (back && !c->p.common.exit);

    return 0;
}
