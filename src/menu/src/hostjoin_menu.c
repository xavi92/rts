#include <menu.h>
#include <menu_private.h>
#include <game.h>
#include <gui.h>
#include <gui/button.h>
#include <gui/container.h>
#include <stdbool.h>

struct menu_hostjoin
{
    struct gui_container cnt;
    struct gui_button host, join, back;
};

static int update(struct menu_common *const c, void *const arg)
{
    struct menu_hostjoin *const m = arg;

    if (gui_update(&m->cnt.common, &c->p, &c->cam, &c->in))
        return -1;

    return 0;
}

static int render(const struct menu_common *const c, void *const arg)
{
    const struct menu_hostjoin *const m = arg;

    if (gui_render(&m->cnt.common))
        return -1;

    return 0;
}

int menu_hostjoin(struct menu_common *const c, bool *const back)
{
    do
    {
        struct menu_hostjoin m;
        bool host = false, join = false;

        gui_container_init(&m.cnt);
        m.cnt.common.hcentered = true;
        m.cnt.common.vcentered = true;
        m.cnt.spacing = 4;
        m.cnt.mode = GUI_CONTAINER_MODE_V;

        gui_button_init(&m.host, GUI_BUTTON_TYPE_1);
        m.host.u.type1.label.text = "Host game";
        m.host.common.hcentered = true;
        m.host.u.type1.w = 140;
        m.host.arg = &host;
        m.host.on_pressed = menu_on_pressed;
        gui_add_child(&m.cnt.common, &m.host.common);

        gui_button_init(&m.join, GUI_BUTTON_TYPE_1);
        m.join.u.type1.label.text = "Join game";
        m.join.common.hcentered = true;
        m.join.u.type1.w = 140;
        m.join.arg = &join;
        m.join.on_pressed = menu_on_pressed;
        gui_add_child(&m.cnt.common, &m.join.common);

        gui_button_init(&m.back, GUI_BUTTON_TYPE_1);
        m.back.u.type1.label.text = "Back";
        m.back.common.hcentered = true;
        m.back.u.type1.w = 140;
        m.back.arg = back;
        m.back.on_pressed = menu_on_pressed;
        gui_add_child(&m.cnt.common, &m.back.common);

        while (!*back && !c->p.common.exit && !host && !join)
        {
            if (menu_update(c, update, render, &m))
                return -1;
        }

        if (host)
        {
            if (menu_gamecfg(c))
                return -1;
        }
        else if (join)
        {
            if (menu_join(c))
                return -1;
        }

    } while (!*back && !c->p.common.exit);

    return 0;
}
