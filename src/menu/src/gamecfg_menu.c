#include <menu.h>
#include <menu_private.h>
#include <game.h>
#include <gui.h>
#include <gui/button.h>
#include <gui/container.h>
#include <gui/rounded_rect.h>
#include <stdbool.h>
#include <stddef.h>

struct gamecfg_menu
{
    struct gui_container cnt, bcnt;
    struct gui_rounded_rect r;
    struct gui_button start, back;
};

static int update(struct menu_common *const c, void *const arg)
{
    struct gamecfg_menu *const m = arg;

    m->bcnt.common.y = screen_h - 40;
    m->r.w = screen_w / 2;
    m->r.h = screen_h / 2;

    if (gui_update(&m->cnt.common, &c->p, &c->cam, &c->in)
        || gui_update(&m->bcnt.common, &c->p, &c->cam, &c->in))
        return -1;

    return 0;
}

static int render(const struct menu_common *const c, void *const arg)
{
    const struct gamecfg_menu *const m = arg;

    if (gui_render(&m->cnt.common)
        || gui_render(&m->bcnt.common))
        return -1;

    return 0;
}

int menu_gamecfg(struct menu_common *const c)
{
    struct gamecfg_menu m;
    bool start = false, back = false;

    gui_container_init(&m.cnt);
    m.cnt.common.hcentered = true;
    m.cnt.common.vcentered = true;

    gui_rounded_rect_init(&m.r);
    gui_add_child(&m.cnt.common, &m.r.common);

    gui_container_init(&m.bcnt);
    m.bcnt.common.hcentered = true;
    m.bcnt.mode = GUI_CONTAINER_MODE_H;
    m.bcnt.spacing = 8;

    gui_button_init(&m.start, GUI_BUTTON_TYPE_1);
    m.start.u.type1.label.text = "Start";
    m.start.u.type1.w = 100;
    m.start.common.vcentered = true;
    m.start.on_pressed = menu_on_pressed;
    m.start.arg = &start;
    m.back.common.vcentered = true;
    gui_add_child(&m.bcnt.common, &m.start.common);

    gui_button_init(&m.back, GUI_BUTTON_TYPE_1);
    m.back.u.type1.label.text = "Back";
    m.back.u.type1.w = 100;
    m.back.common.vcentered = true;
    m.back.on_pressed = menu_on_pressed;
    m.back.arg = &back;
    m.back.common.vcentered = true;
    gui_add_child(&m.bcnt.common, &m.back.common);

    while (!back && !c->p.common.exit && !start)
        if (menu_update(c, update, render, &m))
            return -1;

    if (start)
        return game(NULL);

    return 0;
}
