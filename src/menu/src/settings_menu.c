#include <menu_private.h>
#include <gui.h>
#include <gui/container.h>
#include <gui/label.h>
#include <gui/line_edit.h>
#include <gui/button.h>
#include <stdbool.h>

struct menu_settings
{
    struct gui_container cnt;
    struct gui_button back;
};

static int update(struct menu_common *const c, void *const arg)
{
    struct menu_settings *const m = arg;

    if (gui_update(&m->cnt.common, &c->p, &c->cam, &c->in))
        return -1;

    return 0;
}

static int render(const struct menu_common *const c, void *const arg)
{
    const struct menu_settings *const m = arg;

    if (gui_render(&m->cnt.common))
        return -1;

    return 0;
}

int menu_settings(struct menu_common *const c, bool *const back)
{
    do
    {
        struct menu_settings m;

        gui_container_init(&m.cnt);
        m.cnt.common.hcentered = true;
        m.cnt.common.vcentered = true;
        m.cnt.spacing = 4;
        m.cnt.mode = GUI_CONTAINER_MODE_V;

        gui_button_init(&m.back, GUI_BUTTON_TYPE_1);
        m.back.u.type1.label.text = "Back";
        m.back.common.hcentered = true;
        m.back.u.type1.w = 140;
        m.back.arg = back;
        m.back.on_pressed = menu_on_pressed;
        gui_add_child(&m.cnt.common, &m.back.common);

        while (!*back && !c->p.common.exit)
            if (menu_update(c, update, render, &m))
                return -1;

    } while (!*back && !c->p.common.exit);

    return 0;
}
