#include <pad.h>
#include <pad_private.h>
#include <pad/port.h>
#include <stdbool.h>

bool pad_pressed(const struct pad *const p, const enum pad_key k)
{
    return p->mask & (1 << k);
}

bool pad_justpressed(const struct pad *const p, const enum pad_key k)
{
    return p->mask & (1 << k) && !(p->oldmask & (1 << k));
}

bool pad_released(const struct pad *const p, const enum pad_key k)
{
    return !(p->mask & (1 << k)) && p->oldmask & (1 << k);
}

void pad_update(struct pad *const p)
{
    p->oldmask = p->mask;
    pad_port_update(p);
}

const char *pad_str(const enum pad_key k)
{
    static const char *const s[] =
    {
#define X(x) [x] = #x,
        PAD_KEYS
#undef X
    };

    return s[k];
}
