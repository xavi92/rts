#ifndef PAD_SDL_1_2_H
#define PAD_SDL_1_2_H

#ifdef __cplusplus
extern "C"
{
#endif

struct pad_port
{
    int dummy;
};

#ifdef __cplusplus
}
#endif

#endif /* PAD_SDL_1_2_H */
