#ifndef PAD_H
#define PAD_H

#include <pad/port.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define PAD_KEYS \
    X(PAD_KEY_LEFT) \
    X(PAD_KEY_RIGHT) \
    X(PAD_KEY_UP) \
    X(PAD_KEY_DOWN) \
    X(PAD_KEY_A) \
    X(PAD_KEY_B) \
    X(PAD_KEY_C) \
    X(PAD_KEY_D) \
    X(PAD_KEY_E) \
    X(PAD_KEY_OPTIONS) \
    X(PAD_KEY_FULL_SCREEN) \
    X(PAD_KEY_EXIT)

enum pad_key
{
#define X(x) x,
    PAD_KEYS
#undef X

    MAX_PAD_KEYS
};

struct pad
{
    int player;
    int mask, oldmask;
    struct pad_port port;
};

void pad_init(int player, struct pad *p);
void pad_update(struct pad *p);
bool pad_pressed(const struct pad *p, enum pad_key k);
bool pad_justpressed(const struct pad *p, enum pad_key k);
bool pad_released(const struct pad *p, enum pad_key k);
const char *pad_str(enum pad_key k);

#ifdef __cplusplus
}
#endif

#endif /* PAD_H */
