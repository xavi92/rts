#ifndef PAD_PRIVATE_H
#define PAD_PRIVATE_H

#include <pad.h>

#ifdef __cplusplus
extern "C"
{
#endif

void pad_port_update(struct pad *p);

#ifdef __cplusplus
}
#endif

#endif /* PAD_PRIVATE_H */
