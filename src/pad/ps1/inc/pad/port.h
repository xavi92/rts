#ifndef PAD_PS1_H
#define PAD_PS1_H

#include <psx.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct pad_port
{
    psx_pad_state pps;
};

#ifdef __cplusplus
}
#endif

#endif /* PAD_PS1_H */
