#ifndef TECH_H
#define TECH_H

#ifdef __cplusplus
extern "C"
{
#endif

enum tech_level
{
    TECH_LEVEL_1,
    TECH_LEVEL_2,
    TECH_LEVEL_3
};

#ifdef __cplusplus
}
#endif

#endif /* TECH_H */
