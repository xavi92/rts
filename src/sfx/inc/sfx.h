#ifndef SFX_H
#define SFX_H

#include <sfx/port.h>
#include <stddef.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C"
{
#endif

int sfx_init(void);
int sfx_sound_from_fp(struct sound *s, FILE *f, size_t sz);
int sfx_play(const struct sound *s);
void sfx_free(struct sound *s);
void sfx_deinit(void);

#ifdef __cplusplus
}
#endif

#endif /* SFX_H */
