#ifndef SFX_SDL1_2_H
#define SFX_SDL1_2_H

#include <SDL_mixer.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct sound
{
    Mix_Chunk *chunk;
    bool loop;
};

#ifdef __cplusplus
}
#endif

#endif /* SFX_SDL1_2_H */
