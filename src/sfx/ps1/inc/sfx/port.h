#ifndef SFX_PS1_H
#define SFX_PS1_H

#include <stddef.h>
#include <stdint.h>

typedef uint32_t sfx_spu_addr;
typedef uint8_t sfx_spu_voice;

struct sound
{
    sfx_spu_addr addr;
    uint16_t sample_rate;
    sfx_spu_voice voice;
};

#endif /* SFX_PS1_H */
