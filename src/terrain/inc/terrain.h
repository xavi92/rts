#ifndef TERRAIN_H
#define TERRAIN_H

#include <camera.h>
#include <gfx.h>
#include <util.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

enum
{
    MAP_TILES = 64,
    TERRAIN_SZ = 32,
    MAP_W = MAP_TILES * TERRAIN_SZ,
    MAP_H = MAP_TILES * TERRAIN_SZ
};

enum terrain_type
{
    TERRAIN_TYPE_GRASS
};

struct terrain_map
{
    struct terrain_tile
    {
       enum terrain_type t;
       unsigned char bl;
    } m[MAP_TILES][MAP_TILES];

    int nx, ny, last_w, last_h;
};

void terrain_init(struct terrain_map *map);
void terrain_update(struct terrain_map *map);
int terrain_render(const struct terrain_map *map, const struct camera *cam);
void terrain_block_update(const struct util_rect *dim, bool alive, void *p);

extern struct sprite grass_sprite;

#ifdef __cplusplus
}
#endif

#endif /* TERRAIN_H */
