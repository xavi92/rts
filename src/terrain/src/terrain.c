#include <terrain.h>
#include <camera.h>
#include <gfx.h>
#include <util.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

struct sprite grass_sprite;

void terrain_update(struct terrain_map *const map)
{
    if (map->last_w != screen_w)
    {
        const int extra = !!(screen_w % TERRAIN_SZ);

        map->nx = screen_w / TERRAIN_SZ + extra;
        map->last_w = screen_w;
    }

    if (map->last_h != screen_h)
    {
        const int extra = !!(screen_h % TERRAIN_SZ);

        map->ny = screen_h / TERRAIN_SZ + extra;
        map->last_h = screen_h;
    }
}

struct block
{
    size_t x0, x1, y0, y1;
};

void terrain_block_update(const struct util_rect *const dim, const bool alive,
    void *const p)
{
    const struct block b =
    {
        .x0 = dim->x / TERRAIN_SZ,
        .x1 = (dim->x + dim->w) / TERRAIN_SZ,
        .y0 = dim->y / TERRAIN_SZ,
        .y1 = (dim->y + dim->h) / TERRAIN_SZ
    };

    for (size_t x = b.x0; x <= b.x1; x++)
        for (size_t y = b.y0; y <= b.y1; y++)
            for (size_t i = 0; i < 2; i++)
                for (size_t j = 0; j < 2; j++)
                {
                    const struct util_rect sub =
                    {
                        .x = x * TERRAIN_SZ + i * (TERRAIN_SZ / 2),
                        .y = y * TERRAIN_SZ + j * (TERRAIN_SZ / 2),
                        .w = TERRAIN_SZ / 2,
                        .h = TERRAIN_SZ / 2
                    };

                    if (util_collision(dim, &sub))
                    {
                        const enum
                        {
                            NO_SUB = 0,
                            UPPER_LEFT = 1 << 0,
                            UPPER_RIGHT = 1 << 1,
                            LOWER_LEFT = 1 << 2,
                            LOWER_RIGHT = 1 << 3,
                            ALL_BLOCKS = UPPER_LEFT | UPPER_RIGHT
                                | LOWER_LEFT | LOWER_RIGHT
                        } sub = 1 << (i + j * 2);

                        struct terrain_map *const map = p;
                        unsigned char *const bl = &map->m[y][x].bl;

                        *bl = alive ? *bl | sub : *bl & ~sub;
                    }
                }
}

int terrain_render(const struct terrain_map *const map,
    const struct camera *const cam)
{
    const int start_x = abs(cam->x / TERRAIN_SZ),
        start_y = abs(cam->y / TERRAIN_SZ);

    const int remx = cam->x % TERRAIN_SZ,
        remy = cam->y % TERRAIN_SZ;

    int nx = map->nx, ny = map->ny;

    if (abs(remx))
        nx++;

    if (abs(remy))
        ny++;

    struct m
    {
        size_t i;
        long p;
    };

    for (struct m x = {.i = start_x, .p = remx};
            x.i < nx + start_x; x.i++, x.p += TERRAIN_SZ)
        for (struct m y = {.i = start_y, .p = remy};
            y.i < ny + start_y; y.i++, y.p += TERRAIN_SZ)
        {
            const struct terrain_tile *const t = &map->m[y.i][x.i];

            sprite_get_or_ret(s, -1);

            switch (t->t)
            {
                case TERRAIN_TYPE_GRASS:
                    if (sprite_clone(&grass_sprite, s))
                        return -1;

                    break;
            }

            s->x = x.p;
            s->y = y.p;
            sprite_sort(s);
        }

    return 0;
}

void terrain_init(struct terrain_map *const map)
{
    *map = (const struct terrain_map){0};
}
