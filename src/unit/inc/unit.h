#ifndef UNIT_H
#define UNIT_H

#include <camera.h>
#include <gfx.h>
#include <sfx.h>
#include <instance.h>
#include <resource_type.h>
#include <tech.h>
#include <unit_type.h>
#include <util.h>
#include <fixmath.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct unit_tech
{
    enum tech_level carry;
};

enum unit_state
{
    UNIT_STATE_IDLE_MOVING,
    UNIT_STATE_SHELTERED,
    UNIT_STATE_HARVESTING_WOOD,
    UNIT_STATE_HARVESTING_GOLD,
    UNIT_STATE_CARRYING,
    UNIT_STATE_ATTACKING
};

struct unit_target
{
    struct instance *ins;
    enum unit_state state;
    instance_sheltered_cb shelter;
    instance_attacked_cb attack;
    instance_done_cb done;
    void *op;
};

struct unit
{
    struct instance instance;
    enum unit_type type;

    enum unit_dir
    {
        UNIT_DIR_N,
        UNIT_DIR_NE,
        UNIT_DIR_E,
        UNIT_DIR_SE,
        UNIT_DIR_S,
        UNIT_DIR_SW,
        UNIT_DIR_W,
        UNIT_DIR_NW,

        MAX_UNIT_DIRECTIONS
    } dir;

    enum unit_state state;

    struct
    {
        unsigned char t, i;
    } frame;

    union
    {
        struct unit_harvester
        {
            enum resource_type type;
            unsigned char carry, t;
            struct unit_target prev_target;
        } harvester;
    } us;

    fix16_t rx, ry, tx, ty;

    struct unit_target target;
};

UTIL_STATIC_ASSERT(!offsetof(struct unit, instance), "must be at offset zero");

struct unit_cfg
{
    enum unit_type type;
    unsigned long x, y;
};

void unit_create(const struct unit_cfg *cfg, struct unit *u);
int unit_render(const struct unit *u, const struct camera *cam, bool sel);
bool unit_can_harvest(const struct unit *u);
bool unit_target_valid(const struct unit *u, const struct unit_target *t);
void unit_set_target(struct unit *u, const struct unit_target *t);
void unit_move_to(struct unit *u, unsigned long x, unsigned long y);
bool unit_attacked(struct instance *, instance_hp ap);
void unit_update(const struct unit_tech *t, struct unit *u);
instance_hp unit_maxhp(const struct unit *u);
const char *unit_str(const struct unit *u);

enum
{
    UNIT_SPRITE_N,
    UNIT_SPRITE_NE,
    UNIT_SPRITE_E,
    UNIT_SPRITE_SE,
    UNIT_SPRITE_S,

    MAX_UNIT_SPRIES
};

extern struct sprite unit_sprites[MAX_UNIT_SPRIES];

enum unit_sound
{
    UNIT_SOUND_SELECTED,
    UNIT_SOUND_MOVE,
    UNIT_SOUND_MOVE_2,

    MAX_UNIT_SOUNDS
};

extern struct sound unit_sounds[MAX_UNIT_SOUNDS];

#ifdef __cplusplus
}
#endif

#endif /* UNIT_H */
