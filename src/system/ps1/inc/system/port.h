#ifndef INIT_PS1_H
#define INIT_PS1_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

extern volatile bool vblank_set;

#ifdef __cplusplus
}
#endif

#endif /* INIT_PS1_H */
