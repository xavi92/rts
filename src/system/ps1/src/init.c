#include <gfx.h>
#include <net.h>
#include <sfx.h>
#include <system.h>
#include <psx.h>
#include <stdbool.h>

volatile bool vblank_set;

bool system_can_exit(void)
{
    return false;
}

static void vblank(void *const arg)
{
    vblank_set = true;
}

void system_deinit(void)
{
    gfx_deinit();
    sfx_deinit();
    net_deinit();
}

int system_init(void)
{
    SetVBlankHandler(vblank);

    if (gfx_init() || sfx_init() || net_init())
        return -1;

    return 0;
}

void system_loop(void)
{
}
