#include <gfx.h>
#include <sfx.h>
#include <net.h>
#include <system.h>
#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>

bool system_can_exit(void)
{
    return true;
}

void system_loop(void)
{
    SDL_PumpEvents();
}

void system_deinit(void)
{
    gfx_deinit();
    sfx_deinit();
    net_deinit();
    SDL_Quit();
}

int system_init(void)
{
    if (SDL_Init(0))
    {
        fprintf(stderr, "SDL_Init: %s\n", SDL_GetError());
        goto failure;
    }
    else if (gfx_init() || sfx_init() || net_init())
        goto failure;

    SDL_WM_SetCaption("rts", NULL);
    SDL_ShowCursor(0);
    return 0;

failure:
    system_deinit();
    return -1;
}
