#ifndef SYSTEM_H
#define SYSTEM_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

int system_init(void);
void system_deinit(void);
void system_loop(void);
bool system_can_exit(void);

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_H */
