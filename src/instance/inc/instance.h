#ifndef INSTANCE_H
#define INSTANCE_H

#include <camera.h>
#include <gfx.h>
#include <util.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef unsigned int instance_hp;

struct instance
{
    bool alive, dying;
    unsigned int hp;
    struct util_rect r;
};

typedef bool (*instance_attacked_cb)(struct instance *i, unsigned int ap);
typedef bool (*instance_sheltered_cb)(struct instance *self, struct instance *other);
typedef void (*instance_done_cb)(struct instance *i, void *op);

struct instance_render_cfg
{
    const struct instance *i;
    const struct camera *cam;
    enum
    {
        INSTANCE_RENDER_CFG_SPRITE,
        INSTANCE_RENDER_CFG_QUAD
    } prim_type;

    union
    {
        struct sprite *s;
        const struct instance_render_quad
        {
            struct quad *q;
            short w, h;
            uint8_t u;
            bool xflip;
        } *quad;
    } prim;

    bool sel;
    instance_hp max_hp;
    const struct instance_render_off
    {
        short x, y;
    } *off;
};

bool instance_attacked(struct instance *self, unsigned int ap);
void instance_clear_pools(void);
int instance_render(const struct instance_render_cfg *cfg);
int instance_render_target(const struct instance *const i, const struct camera *cam);
void instance_cyclic(void);

#ifdef __cplusplus
}
#endif

#endif /* INSTANCE_H */
