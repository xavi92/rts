#ifndef BUILDING_TYPE_H
#define BUILDING_TYPE_H

enum building_type
{
    BUILDING_TYPE_BARRACKS,

    MAX_BUILDING_TYPES
};

#endif /* BUILDING_TYPE_H */
