#ifndef BUILDING_H
#define BUILDING_H

#include <building_type.h>
#include <camera.h>
#include <gfx.h>
#include <instance.h>
#include <util.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct building
{
    struct instance instance;
    enum building_type type;
};

UTIL_STATIC_ASSERT(!offsetof(struct building, instance), "must be at offset zero");

struct building_cfg
{
    enum building_type type;
    unsigned long x, y;
};

void building_create(const struct building_cfg *cfg, struct building *b);
void building_set_alive_cb(void (*f)(const struct util_rect *dim, bool alive, void *p), void *p);
int building_render(const struct building *b, const struct camera *cam, bool sel);
instance_hp building_maxhp(const struct building *b);
const char *building_str(const struct building *b);

extern struct sprite building_sprites[MAX_BUILDING_TYPES];

#ifdef __cplusplus
}
#endif

#endif /* BUILDING_H */
