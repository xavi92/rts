#include <util.h>
#include <stdbool.h>

bool util_collision(const struct util_rect *const a, const struct util_rect *const b)
{
    return !((a->x >= b->x + b->w)
        || (b->x >= a->x + a->w)
        || (a->y >= b->y + b->h)
        || (b->y >= a->y + a->h));
}
