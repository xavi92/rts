#ifndef UTIL_H
#define UTIL_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

#if __STDC_VERSION__ >= 201112L
#define UTIL_STATIC_ASSERT(exp, msg) _Static_assert(exp, msg)
#else
#define UTIL_STATIC_ASSERT__(exp, l, msg) enum {static_assert##l##__ = 1 / !!(exp)}
#define UTIL_STATIC_ASSERT_(exp, l, msg) UTIL_STATIC_ASSERT__(exp, l, msg)
#define UTIL_STATIC_ASSERT(exp, msg) UTIL_STATIC_ASSERT_(exp, __LINE__, msg)
#endif

struct util_rect
{
    unsigned long x, y;
    short w, h;
};

bool util_collision(const struct util_rect *a, const struct util_rect *b);

#ifdef __cplusplus
}
#endif

#endif /* UTIL_H */
