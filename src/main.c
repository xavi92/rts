#include <menu.h>
#include <system.h>
#include <stdlib.h>

int main(void)
{
    int ret = EXIT_SUCCESS;

    if (system_init() || menu())
        ret = EXIT_FAILURE;

    system_deinit();
    return ret;
}
