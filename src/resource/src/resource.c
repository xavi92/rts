#include <resource.h>
#include <gfx.h>
#include <instance.h>
#include <stdbool.h>
#include <stddef.h>

struct sprite resource_sprites[MAX_RESOURCE_TYPES];
static void (*cb)(const struct util_rect *, bool, void *);
static void *op;

static bool gold_shelter(struct instance *const self,
    struct instance *const other)
{
    struct resource *const res = (struct resource *)self;
    struct resource_gold *const g = &res->res.gold;

    for (size_t i = 0; i < sizeof g->miners / sizeof *g->miners; i++)
    {
        struct instance **const ins = &g->miners[i];

        if (!*ins)
        {
            *ins = other;
            g->n_miners++;
            return true;
        }
    }

    return false;
}

instance_sheltered_cb resource_shelter(const struct resource *res)
{
    static const instance_sheltered_cb s[] =
    {
        [RESOURCE_TYPE_GOLD] = gold_shelter
    };

    return s[res->type];
}

bool resource_harvested(struct instance *const self, const instance_hp ap)
{
    const bool ret = instance_attacked(self, ap);

    if (ret && cb)
        cb(&self->r, self->alive, op);

    return ret;
}

instance_hp resource_maxhp(const struct resource *const res)
{
    static const instance_hp hp[] =
    {
        [RESOURCE_TYPE_GOLD] = 1000,
        [RESOURCE_TYPE_WOOD] = 45
    };

    return hp[res->type];
}

int resource_render(const struct resource *const res,
    const struct camera *const cam, const bool sel)
{
    const struct instance *const in = &res->instance;

    if (!in->alive)
        return 0;

    sprite_get_or_ret(s, -1);

    if (sprite_clone(&resource_sprites[res->type], s))
        return -1;

    const struct instance_render_off *off = NULL;

    switch (res->type)
    {
        case RESOURCE_TYPE_GOLD:
            s->h = in->r.h;

            if (res->res.gold.n_miners)
                s->v += s->h;

            break;

        case RESOURCE_TYPE_WOOD:
        {
            static const struct instance_render_off w_off =
            {
                .y = -30
            };

            off = &w_off;
        }
            break;

        default:
            break;
    }

    const struct instance_render_cfg cfg =
    {
        .i = &res->instance,
        .prim_type = INSTANCE_RENDER_CFG_SPRITE,
        .prim = {.s = s},
        .cam = cam,
        .sel = sel,
        .max_hp = resource_maxhp(res),
        .off = off
    };

    return instance_render(&cfg);
}

static void get_dimensions(const enum resource_type type, short *const w,
    short *const h)
{
    static const struct dim
    {
        short w, h;
    } dim[] =
    {
        [RESOURCE_TYPE_GOLD] = {.w = 96, .h = 96},
        [RESOURCE_TYPE_WOOD] = {.w = 32, .h = 16}
    };

    const struct dim *const d = &dim[type];
    *w = d->w;
    *h = d->h;
}

int resource_create(const struct resource_cfg *const cfg, struct resource *const list,
    const size_t n)
{
    for (size_t i = 0; i < n; i++)
    {
        struct resource *const r = &list[i];
        struct instance *const in = &r->instance;

        if (!in->alive)
        {
            get_dimensions(cfg->type, &in->r.w, &in->r.h);
            r->type = cfg->type;
            in->r.x = cfg->x;
            in->r.y = cfg->y;
            in->alive = true;
            in->hp = resource_maxhp(r);

            if (cb)
                cb(&in->r, in->alive, op);

            return 0;
        }
    }

    return -1;
}

void resource_set_alive_cb(void (*const f)(const struct util_rect *, bool, void *),
    void *const p)
{
    cb = f;
    op = p;
}

const char *resource_str(const struct resource *const res)
{
    static const char *const str[] =
    {
        [RESOURCE_TYPE_GOLD] = "Gold mine",
        [RESOURCE_TYPE_WOOD] = "Pine tree"
    };

    return str[res->type];
}
