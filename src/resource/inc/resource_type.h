#ifndef RESOURCE_TYPE_H
#define RESOURCE_TYPE_H

#ifdef __cplusplus
extern "C"
{
#endif

enum resource_type
{
    RESOURCE_TYPE_WOOD,
    RESOURCE_TYPE_GOLD,

    MAX_RESOURCE_TYPES
};

#ifdef __cplusplus
}
#endif

#endif /* RESOURCE_TYPE_H */
