#ifndef RESOURCE_H
#define RESOURCE_H

#include <camera.h>
#include <container.h>
#include <gfx.h>
#include <instance.h>
#include <resource_type.h>
#include <util.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

enum {RESOURCE_GOLD_CAPACITY = 2};

struct resource
{
    struct instance instance;
    enum resource_type type;

    union
    {
        struct resource_gold
        {
            struct instance *miners[RESOURCE_GOLD_CAPACITY];
            size_t n_miners;
        } gold;
    } res;
};

UTIL_STATIC_ASSERT(!offsetof(struct resource, instance), "must be at offset zero");

struct resource_cfg
{
    enum resource_type type;
    unsigned long x, y;
};

const struct container_list *resource_res(void);
void resource_set_alive_cb(void (*f)(const struct util_rect *dim, bool alive, void *p), void *p);
int resource_create(const struct resource_cfg *cfg, struct resource *list, size_t n);
int resource_render(const struct resource *res, const struct camera *cam, bool sel);
instance_sheltered_cb resource_shelter(const struct resource *res);
bool resource_harvested(struct instance *i, instance_hp ap);
instance_hp resource_maxhp(const struct resource *res);
const char *resource_str(const struct resource *res);

extern struct sprite resource_sprites[MAX_RESOURCE_TYPES];

#ifdef __cplusplus
}
#endif

#endif /* RESOURCE_H */
