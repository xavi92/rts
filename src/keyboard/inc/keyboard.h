#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <keyboard_key.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define KEYBOARD_COMBO(...) (const struct keyboard_combo){.keys = {__VA_ARGS__}}

enum {KEYBOARD_MAX_COMBO_KEYS = 3};

struct keyboard
{
    struct keyboard_combo
    {
        enum keyboard_key keys[KEYBOARD_MAX_COMBO_KEYS];
    } combo, oldcombo;

    size_t i;
};

void keyboard_init(struct keyboard *k);
void keyboard_update(struct keyboard *k);
bool keyboard_justpressed(const struct keyboard *k, const struct keyboard_combo *c);
bool keyboard_pressed(const struct keyboard *k, const struct keyboard_combo *c);
bool keyboard_justreleased(const struct keyboard *k, const struct keyboard_combo *c);
bool keyboard_any_justpressed(const struct keyboard *k, struct keyboard_combo *c);
bool keyboard_any_pressed(const struct keyboard *k, struct keyboard_combo *c);
char keyboard_to_char(const struct keyboard *k, enum keyboard_key key);
const char *keyboard_key_str(enum keyboard_key k);

#ifdef __cplusplus
}
#endif

#endif /* KEYBOARD_H */
