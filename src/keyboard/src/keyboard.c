#include <keyboard.h>
#include <keyboard_key.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

bool keyboard_any_justpressed(const struct keyboard *const k,
    struct keyboard_combo *const c)
{
    bool ret = false;

    for (size_t i = 0; i < sizeof c->keys / sizeof c->keys; i++)
        c->keys[i] = KEYBOARD_KEY_NONE;

    for (size_t i = 0, j = 0;
        i < sizeof k->combo.keys / sizeof *k->combo.keys; i++)
    {
        const enum keyboard_key key = k->combo.keys[i];

        if (key != KEYBOARD_KEY_NONE
            && k->oldcombo.keys[i] == KEYBOARD_KEY_NONE)
        {
            c->keys[j++] = key;
            ret = true;
        }
    }

    return ret;
}

bool keyboard_any_pressed(const struct keyboard *const k,
    struct keyboard_combo *const c)
{
    for (size_t i = 0; i < sizeof k->combo.keys / sizeof *k->combo.keys; i++)
    {
        const enum keyboard_key key = k->combo.keys[i];

        if (key != KEYBOARD_KEY_NONE)
        {
            *c = k->combo;
            return true;
        }
    }

    return false;
}

char keyboard_to_char(const struct keyboard *const k,
    const enum keyboard_key key)
{
    if (key >= KEYBOARD_KEY_0 && key <= KEYBOARD_KEY_9)
        return '0' + key - KEYBOARD_KEY_0;
    else if (key >= KEYBOARD_KEY_A && key <= KEYBOARD_KEY_Z)
    {
        if (keyboard_pressed(k, &KEYBOARD_COMBO(KEYBOARD_KEY_LSHIFT))
            || keyboard_pressed(k, &KEYBOARD_COMBO(KEYBOARD_KEY_RSHIFT)))
            return 'A' + key - KEYBOARD_KEY_A;
        else
            return 'a' + key - KEYBOARD_KEY_A;
    }
    else
    {
        static const struct map
        {
            enum keyboard_key key;
            char ch;
        } map[] =
        {
            {.key = KEYBOARD_KEY_DOT, .ch = '.'},
            {.key = KEYBOARD_KEY_SPACE, .ch = ' '},
            {.key = KEYBOARD_KEY_MINUS, .ch = '-'},
            {.key = KEYBOARD_KEY_SLASH, .ch = '/'}
        };

        for (size_t i = 0; i < sizeof map / sizeof *map; i++)
        {
            const struct map *const m = &map[i];

            if (key == m->key)
                return m->ch;
        }
    }

    return -1;
}

static bool combo_pressed(const struct keyboard_combo *const ref,
        const struct keyboard_combo *const c)
{
    bool ret = false;

    for (size_t i = 0; i < sizeof c->keys / sizeof *c->keys; i++)
    {
        const enum keyboard_key key = c->keys[i];

        if (key != KEYBOARD_KEY_NONE)
        {
            bool success = false;

            for (size_t i = 0; i < sizeof ref->keys / sizeof *ref->keys; i++)
            {
                if (key == ref->keys[i])
                {
                    success = true;
                    break;
                }
            }

            if (!(ret = success))
                break;
        }
    }

    return ret;
}

bool keyboard_pressed(const struct keyboard *const k,
    const struct keyboard_combo *const c)
{
    return combo_pressed(&k->combo, c);
}

bool keyboard_justreleased(const struct keyboard *const k,
    const struct keyboard_combo *const c)
{
    return !combo_pressed(&k->combo, c) && combo_pressed(&k->oldcombo, c);
}

bool keyboard_justpressed(const struct keyboard *const k,
    const struct keyboard_combo *const c)
{
    return combo_pressed(&k->combo, c) && !combo_pressed(&k->oldcombo, c);
}

void keyboard_init(struct keyboard *const k)
{
    *k = (const struct keyboard){0};
}

const char *keyboard_key_str(const enum keyboard_key k)
{
    static const char *const s[] =
    {
#define X(x) [x] = #x,
        KEYBOARD_KEYS
#undef X
    };

    return s[k];
}
