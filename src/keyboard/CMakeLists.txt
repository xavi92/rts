set(src "src/keyboard.c")
set(inc "inc")

if(PS1_BUILD)
    set(src ${src} "ps1/src/keyboard.c")
    set(privdeps ${privdeps} PSXSDK::PSXSDK)
elseif(SDL1_2_BUILD)
    set(src ${src} "sdl-1.2/src/keyboard.c")
    set(privdeps ${privdeps} SDL::SDL)
endif()

add_library(keyboard ${src})
target_include_directories(keyboard PUBLIC ${inc})
target_link_libraries(keyboard PUBLIC ${deps} PRIVATE ${privdeps})
