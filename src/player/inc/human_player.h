#ifndef HUMAN_PLAYER_H
#define HUMAN_PLAYER_H

#include <camera.h>
#include <keyboard.h>
#include <instance.h>
#include <input.h>
#include <mouse.h>
#include <pad.h>
#include <peripheral.h>
#include <player.h>
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

enum {MAX_SELECTED_INSTANCES = 4};

struct human_player_cfg
{
    enum peripheral_type sel_periph;
    struct player_cfg pl;
    int padn;
    struct camera_dim dim;
};

struct human_player
{
    struct player pl;
    struct camera cam;
    union peripheral periph;
    struct input in;

    struct sel_instance
    {
        enum sel_type
        {
            INSTANCE_TYPE_UNIT,
            INSTANCE_TYPE_BUILDING,
            INSTANCE_TYPE_RESOURCE
        } type;

        union sel_data
        {
            const struct unit *u;
            const struct building *b;
            const struct resource *r;
            const struct instance *i;
            struct unit *rw_u;
        } d;
    } sel[MAX_SELECTED_INSTANCES];

    struct human_player_target
    {
        const struct instance *ins;
        unsigned char t, n;
        bool render;
    } target;

    size_t n_sel;
    bool top_gui;
    uint32_t gui_res[MAX_RESOURCE_TYPES];
};

int human_player_init(const struct human_player_cfg *cfg, struct human_player *h);
void human_player_update(struct human_player *h, struct player_others *o);
int human_player_render(const struct human_player *h, const struct player_others *o);

#ifdef __cplusplus
}
#endif

#endif /* HUMAN_PLAYER_H */
