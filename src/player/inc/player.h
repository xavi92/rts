#ifndef PLAYER_H
#define PLAYER_H

#include <building.h>
#include <camera.h>
#include <instance.h>
#include <pad.h>
#include <resource.h>
#include <tech.h>
#include <unit.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef unsigned int player_team;

enum
{
    PLAYER_MAX_UNITS = 5,
    PLAYER_MAX_BUILDINGS = 5
};

struct player
{
    enum player_color
    {
        PLAYER_COLOR_BLUE,
        PLAYER_COLOR_RED
    } color;

    player_team team;
    bool alive;
    struct unit units[PLAYER_MAX_UNITS];
    struct building buildings[PLAYER_MAX_BUILDINGS];
    uint32_t resources[MAX_RESOURCE_TYPES];
    unsigned char pop, bpop;

    struct
    {
        struct unit_tech u;
    } tree;
};

struct player_cfg
{
    enum player_color color;
    player_team team;
    unsigned long x, y;
};

struct player_others
{
    struct player *pl;
    struct resource *res;
    size_t n_pl, n_res;
};

int player_init(const struct player_cfg *cfg, struct player *pl);
int player_create_unit(const struct unit_cfg *cfg, struct player *pl);
int player_create_building(const struct building_cfg *cfg, struct player *pl);
void player_update(struct player *p);

#ifdef __cplusplus
}
#endif

#endif /* PLAYER_H */
