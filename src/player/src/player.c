#include <player.h>
#include <building.h>
#include <unit.h>
#include <stddef.h>

void player_update(struct player *const p)
{
    for (size_t i = 0; i < sizeof p->units / sizeof *p->units; i++)
    {
        struct unit *const u = &p->units[i];

        unit_update(&p->tree.u, u);
    }
}

int player_create_unit(const struct unit_cfg *const cfg, struct player *const pl)
{
    enum {N = sizeof pl->units / sizeof *pl->units};

    if (pl->pop < N)
    {
        for (size_t i = 0; i < N; i++)
        {
            struct unit *const u = &pl->units[i];
            struct instance *const i = &u->instance;

            if (!i->alive)
            {
                unit_create(cfg, u);
                pl->pop++;
                return 0;
            }
        }
    }

    return -1;
}

int player_create_building(const struct building_cfg *const cfg, struct player *const pl)
{
    enum {N = sizeof pl-> buildings / sizeof *pl->buildings};

    if (pl->bpop < N)
    {
        for (size_t i = 0; i < N; i++)
        {
            struct building *const b = &pl->buildings[i];
            struct instance *const i = &b->instance;

            if (!i->alive)
            {
                building_create(cfg, b);
                pl->bpop++;
                return 0;
            }
        }
    }

    return -1;
}

int player_init(const struct player_cfg *const cfg, struct player *const pl)
{
    const unsigned long x = cfg->x, y = cfg->y;

    const struct building_cfg bcfg =
    {
        .type = BUILDING_TYPE_BARRACKS,
        .x = x,
        .y = y
    };

    const struct unit_cfg cfgs[] =
    {
        {
            .type = UNIT_TYPE_PEASANT,
            .x = x + 80,
            .y = y
        },

        {
            .type = UNIT_TYPE_PEASANT,
            .x = x + 80,
            .y = y + 40
        },

        {
            .type = UNIT_TYPE_PEASANT,
            .x = x + 100,
            .y = y + 20
        }
    };

    if (player_create_building(&bcfg, pl))
        return -1;

    for (size_t i = 0; i < sizeof cfgs / sizeof *cfgs; i++)
        if (player_create_unit(&cfgs[i], pl))
            return -1;

    for (size_t i = 0; i < sizeof pl->resources / sizeof *pl->resources; i++)
    {
        enum {DEFAULT_RES = 120};

        pl->resources[i] = DEFAULT_RES;
    }

    pl->alive = true;
    pl->color = cfg->color;
    pl->team = cfg->team;
    return 0;
}
