add_library(player
    "src/player.c"
    "src/human_player.c"
    "src/human_player_gui.c"
)
target_include_directories(player PUBLIC "inc" PRIVATE "privinc")
target_link_libraries(player
    PUBLIC
        building
        camera
        gfx
        keyboard
        instance
        input
        mouse
        pad
        resource
        tech
        unit
        util
    PRIVATE
        pad
        gui)
