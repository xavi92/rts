#ifndef HUMAN_PLAYER_PRIVATE_H
#define HUMAN_PLAYER_PRIVATE_H

#include <human_player.h>

#ifdef __cplusplus
extern "C"
{
#endif

void human_player_gui_update(struct human_player *h);
int human_player_gui_render(const struct human_player *h);

#ifdef __cplusplus
}
#endif

#endif /* HUMAN_PLAYER_PRIVATE_H */
