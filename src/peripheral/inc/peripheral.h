#ifndef PERIPHERAL_H
#define PERIPHERAL_H

#include <pad.h>
#include <keyboard.h>
#include <mouse.h>
#include <util.h>
#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

enum peripheral_type
{
    PERIPHERAL_TYPE_PAD,
    PERIPHERAL_TYPE_TOUCH,
    PERIPHERAL_TYPE_KEYBOARD_MOUSE
};

union peripheral
{
    struct peripheral_common
    {
        enum peripheral_type type;
        bool exit;
    } common;

    struct peripheral_pad
    {
        struct peripheral_common common;
        struct pad pad;
    } pad;

    struct peripheral_kbm
    {
        struct peripheral_common common;
        struct mouse mouse;
        struct keyboard keyboard;
        bool long_press;
        unsigned int lp_t;
    } kbm;
};

struct peripheral_cfg
{
    enum peripheral_type type;
    int padn;
};

UTIL_STATIC_ASSERT(!offsetof(struct peripheral_pad, common),
    "unexpected offsetof for struct peripheral_pad");
UTIL_STATIC_ASSERT(!offsetof(struct peripheral_kbm, common),
    "unexpected offsetof for struct peripheral_kbm");

void peripheral_init(const struct peripheral_cfg *cfg, union peripheral *p);
void peripheral_input_set(union peripheral *p, void (*cb)(char, void *));
void peripheral_update(union peripheral *p);

#ifdef __cplusplus
}
#endif

#endif /* PERIPHERAL_H */
