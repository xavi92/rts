#ifndef INPUT_H
#define INPUT_H

#include <keyboard.h>
#include <mouse.h>
#include <pad.h>
#include <peripheral.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef void (*input_ch)(char ch, void *user);
typedef void (*input_erase)(void *user);

struct input
{
    input_ch cb;
    input_erase erase;
    void *user;
    unsigned char t;
    bool repeat;
    struct keyboard_combo prev;
};

void input_update(struct input *in, const union peripheral *p);
int input_render(const struct input *in, const union peripheral *p);
bool input_keyboard_justpressed(const struct input *in,
    const struct keyboard *k,
    const struct keyboard_combo *c);
bool input_keyboard_pressed(const struct input *in,
    const struct keyboard *k,
    const struct keyboard_combo *c);
bool input_keyboard_justreleased(const struct input *in,
    const struct keyboard *k,
    const struct keyboard_combo *c);
bool input_pad_pressed(const struct input *in, const struct pad *p,
    enum pad_key k);
bool input_pad_justpressed(const struct input *in, const struct pad *p,
    enum pad_key k);
bool input_pad_released(const struct input *in, const struct pad *p,
    enum pad_key k);
bool input_mouse_pressed(const struct input *in, const struct mouse *m,
    enum mouse_button b);
bool input_mouse_justpressed(const struct input *in, const struct mouse *m,
    enum mouse_button b);
bool input_mouse_justreleased(const struct input *in, const struct mouse *m,
    enum mouse_button b);

#ifdef __cplusplus
}
#endif

#endif /* INPUT_H */
