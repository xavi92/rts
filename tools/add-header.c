#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **const argv)
{
    int ret = EXIT_FAILURE;
    FILE *in = NULL, *out = NULL;

    if (argc < 3)
    {
        fprintf(stderr, "%s [arg ...] <in-file> <out-file>\n", *argv);
        goto end;
    }

    const char *const in_path = argv[argc - 2],
        *const out_path = argv[argc - 1];

    if (!(out = fopen(out_path, "wb")))
    {
        fprintf(stderr, "could not open %s: %s\n", out_path, strerror(errno));
        goto end;
    }
    else if (!(in = fopen(in_path, "rb")))
    {
        fprintf(stderr, "could not open %s: %s\n", in_path, strerror(errno));
        goto end;
    }

    for (int i = 1; i < argc - 2; i++)
    {
        if (fprintf(out, "%s", argv[i]) < 0
            || putc('\0', out) == EOF
            || ferror(out))
        {
            fprintf(stderr, "failed writing to %s\n", out_path);
            goto end;
        }
    }

    while (!feof(in))
    {
        char c;

        if ((!fread(&c, sizeof c, 1, in)
            || !fwrite(&c, sizeof c, 1, out))
            && (ferror(in) || ferror(out)))
        {
            fprintf(stderr, "ferror(%s)=%d, ferror(%s)=%d\n",
                in_path, ferror(in), out_path, ferror(out));
            goto end;
        }
    }

    ret = EXIT_SUCCESS;

end:
    if (out)
        fclose(out);

    if (in)
        fclose(in);

    return ret;
}
