# Cross-compilation for Win9x
Creating a toolchain targetting `i386-mingw32` is not usually possible
with the distribution-provided packages. This documentation describes
which versions are known to work so far, as well as detailed
configuration scripts.

The following documentation assumes the installation prefix
`$HOME/i386-mingw32` for the GNU toolchain. However, feel free to
change this value, if required.

Some specific library versions might be hard to find, so download links
are provided.

## Environment variables
Since it is desirable to avoid messing with system-level libraries,
when building for Win9x, `rts` will look for dependencies by inspecting
the following environment variables:

- `SDL_PATH`
- `SDL_TTF_PATH`
- `FREETYPE_PATH`
- `SDL_MIXER_PATH`

Also, the directory containing the GNU toolchain binaries (which
should be located at `$HOME/i386-mingw32/bin` by default) must be added
to the `PATH` variable.

## gcc
From own experience, modern versions of `gcc` cannot be used to compile
a cross-compiler for Win9x. Instead, a *much* older version is required.
So far, `gcc-3.4.5` is the latest known version to work.

### Known issues
- Take into account only C99 is supported by `gcc-3.4.5`.
- `gcc-3.4.5` complains about the use of `-ffunction-sections` and
`-fdata-sections`.

```sh
../gcc-3.4.5-20060117-2/configure --disable-nls --disable-rpath \
    --disable-win32-registry --enable-languages=c --enable-sjlj-exceptions \
    --enable-threads=win32 --prefix=$HOME/i386-mingw32 \
    --program-prefix=i386-mingw32- --target=i386 -mingw32 --with-gnu-as \
    --with-gnu-ld
```

[gcc-core-3.4.5-20060117-2-src.tar.gz](https://sourceforge.net/proejcts/mingw/files/MinGW/Base/gcc/Version3/Previous%20Release_%20gcc-3.45-20060117-2/gcc-core-3.4.5-20060117-2-src.tar.gz)

## binutils
GNU binutils is usually not so problematic, so any version should do.
Only `binutils-2.35` has been tested.

```sh
../binutils-2.35/configure --prefix=$HOME/i386-mingw32 --target=i386-mingw32
```
## mingwrt and w32api
So far, only versions `mingwrt-3.15.2-mingw32` and `w32api-3.13-mingw32`
have been tested.

### Known issues
- Some hacks have to be done in order to compile these libraries. These
are described on the script below.

```sh
../w32api-3.13-mingw32/configure --prefix=$HOME/i386-mingw32 --host=i386-mingw32
# Hack at line below
cd mingwrt-3.15.2-mingw32/ && cp -r include/* ~/i386-mingw32/include/
../mingwrt-3.15.2-mingw32/configure --prefix=$HOME/i386-mingw32 --target=i386-mingw32
../sdl-1.2.15-src/configure --host=i386-mingw32 --prefix=/home/xavier/sdl-1.2.15 \
    --enable-shared=no CC=i386-mingw32-gcc
```

[mingw-runtime-3.15.2](https://sourceforge.net/projects/mingw/files/OldFiles/mingwrt-3.15.2/mingwrt-3.15.2-mingw32-src.tar.gz)

[mingw-w32api-3.13](https://sourceforge.net/projects/mingw/files/OldFiles/w32api-3.13/w32api-3.13-mingw32-src.tar.gz)

## freetype (not required)
Only `freetype-2.4.8` is known to work so far.

### Known issues
- Apparently, out of source builds are not possible.

```sh
cd freetype-2.4.8-src/ && ./configure --prefix=$HOME/freetype-2.4.8 \
        --host=i386-mingw32 --enable-shared=no CC=i386-mingw32-gcc
```

## SDL
`sdl-1.2.15` is the latest version supported version for the Win9x family
of operating systems. In order to keep things easy, the script below
configures SDL as a static library.

```sh
../sdl-1.2.15-src/configure --host=i386-mingw32 --prefix=$HOME/sdl-1.2.15 \
    --enable-shared=no CC=i386-mingw32-gcc \
    CFLAGS='-ffunction-sections -fdata-sections'
```

### DirectX headers and static libraries
DirectX can help with hardware acceleration on older hardware. Fortunately,
the SDL team distribute both source and binaries that can be included when
building `sdl-1.2.15`. While two tarballs are distributed, only
`directx-devel.tar.gz` has been tested so far.

[directx-devel.tar.gz](https://www.libsdl.org/extras/win32/common/directx-devel.tar.gz)
[directx-source.tar.z](https://www.libsdl.org/extras/win32/common/directx-source.tar.gz)

```sh
../sdl-1.2.15-src/configure --host=i386-mingw32 --prefix=$HOME/sdl-1.2.15 \
    --enable-shared=no CC=i386-mingw32-gcc \
    CFLAGS="-ffunction-sections -fdata-sections \
    -I$HOME/directx-devel/include -L$HOME/directx-devel/lib"
```

In order to take advantage of DirectX, remember to assign the environment
variable `SDL_VIDEODRIVER` to `directx` before running `rts`.

## SDL_mixer
Only `SDL_mixer-1.2.12` is known to work so far. Since `rts` only uses
WAVE files, support for other audio formats is not required.

```sh
../SDL_mixer-1.2.12-src/configure --host=i386-mingw32 --enable-shared=no \
    --prefix=$HOME/SDL_mixer-1.2.12 --disable-music-cmd --disable-music-mod \
    --disable-music-ogg --disable-music-mp3 --disable-music-midi \
    --disable-music-flac CC=i386-mingw32-gcc --with-sdl-prefix=$HOME/sdl-1.2.15 \
    CFLAGS='-ffunction-sections -fdata-sections'
```

```sh
../SDL_gfx-2.0.26-src/configure --host=i386-mingw32 --enable-shared=no \
    --prefix=$HOME/SDL_gfx-2.0.26 --with-sdl-prefix=$HOME/sdl-1.2.15 \
    CFLAGS='-ffunction-sections -fdata-sections' \
    CC=i386-mingw32-gcc
```

## ENET

```sh
../enet-1.3.17/configure --prefix=$HOME/enet-1.3.17-win32 \
    --host=i386-mingw32 CFLAGS='-ffunction-sections -fdata-sections' \
    CC=i386-mingw32-gcc
```
