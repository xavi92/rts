# Contributing
## Overview
`rts` strives to run on a wide variety of platforms, including low-end
and old hardware, such as PSX or even `i486` computers. This is possible
thanks to the use of portable source code and the wide availability of
`sdl-1.2`. However, `rts` should be easy to extend to other platforms.

## License
As stated on [the project license](../LICENSE), `rts` is licensed by
the GNU General Public License, either version 3 of the license, or
later. Anyone contributing their own source code to this project must
adhere to this license. Also, only source code from other projects
compatible with the project license is allowed.

## C language revision
Because of C11 not being available for the `i386-mingw32` toolchain, C99
is the selected language revision for this project. On the other hand,
feel free to use any helpful C99 features.

## Compiler extensions
Avoid compiler-specific extensions unless absolutely needed. While this
project is currently built using the GNU toolchain, those using other
compilers might appreciate not having to deal with such extensions.

## Platform-specific code
Separate platform-agnostic code from platform-specific code. The usual
method used by this project is to separate platform-specific code into
its own directory. Please, do not use macros for this purpose, since
they make code more difficult to read and maintain.

This is the typical tree structure of a module:

```
module_name/
├── CMakeLists.txt
├── inc
│   └── file.h
├── privinc
│   └── file.h
├── ps1
│   ├── inc
│   │   └── module_name
│   │       └── file.h
│   ├── privinc
│   │   └── ps1
│   │       └── file.h
│   └── src
│       └── file.c
└── sdl-1.2
    ├── inc
    │   └── module_name
    │       └── file.h
    ├── privinc
    │   └── sdl-1.2
    │       └── file.h
    └── src
        └── file.c
```

Where:

- `src` holds the source files defining the implementation of a given module.
- `inc` holds public header files accessible to other modules.
- `privinc` holds public header files accessible to other modules.
- `ps1` and `sdl-1.2` are two of the platforms supported by this project.

## C code conventions

- Use Allman style for brackets.
- Strive for maximum `const`-correctness.
- Keep a hard 80-column limit.
