# RTS (working title)

## Overview

**RTS** is a cross-platform, sprite-based real-time strategy video game
in the likes of several other entries from the mid 90's.

**The game is under heavy development, and very far from being playable.**

![Screencast of in-game footage](demo.mp4)

## Platforms

The following platforms are either supported or support is expected in
the future:

- Sony® PlayStation® 1, using a forked version of
[PSXSDK](https://git.disroot.org/xavi92/psxsdk).
- Microsoft® Win9x, using a `i386-mingw32` cross-toolchain and SDL-1.2.
A `i386-mingw32` cross-toolchain must be already available on the
system.
- POSIX-compliant operating systems such as GNU/Linux® or *BSD, using
SDL-1.2 (if available).
- Possibly, modern Microsoft® Windows® versions, too (currently
untested).

## Design goals

- Keep source code as simple and clean as possible.
- Keep platform-specific code separated so common code can be shared
between platforms without modifications.
- Use C99 features for better portability.
- Use modern CMake features for a simplified build process and
availability of the `compile_commands.json` database.
- Implement any multiplayer capabilities provided by the platform.
- Support a wide range of screen resolutions, even portrait resolutions
typically used by mobile platforms.
- And, above all, provide a fun game that can run even on low-end
hardware.

## Building from source

### Native build

A native version of **RTS** can be built using the typical CMake build
process:

```sh
mkdir build
cd build
cmake ..
make -j$(nproc --all)
```

#### Dependencies

##### Ubuntu

```sh
apt install libsdl-gfx1.2-dev libsdl-mixer1.2-dev libsdl1.2-dev
```

### Cross-compilation

[`CMAKE_TOOLCHAIN_FILE`](https://cmake.org/cmake/help/latest/variable/CMAKE_TOOLCHAIN_FILE.html)
can be used to set up the cross-toolchain. Files labeled as
`cmake/*-toolchain.cmake` can be used as values.

#### Sony® PlayStation® 1

For example, the Sony® PlayStation® 1 version can be built using:

```sh
mkdir build
cd build
cmake .. \
    -DCMAKE_TOOLCHAIN_FILE=../cmake/ps1-toolchain.cmake \
    -DVIDEO_MODE=VMODE_PAL # VMODE_NTSC can be otherwise used
make -j$(nproc --all)
```

This will generate a `.bin`/`.cue` file pair in `build` that can be
played on an emulator or burnt into a CD-r in order to play the game
on real hardware.

#### Microsoft® Win9x

```sh
mkdir build
cd build
SDLDIR=<sdl-prefix> \
    SDLMIXERDIR=<sdl_mixer-prefix> \
    SDLGFXDIR=<sdl_gfx-prefix> \
    ENETDIR=<enet-prefix> \
    cmake .. -DCMAKE_TOOLCHAIN_FILE=../cmake/win9x-toolchain.cmake
make -j$(nproc --all)
```

Where:

- `SDLDIR` is the path to the cross-compiled version for `SDL-1.2`, which
would correspond to `./configure --prefix=$SDLDIR` used when building
`SDL-1.2`.
- `SDLMIXERDIR` is the path to the cross-compiled version for `SDL_mixer`,
which would correspond to `./configure --prefix=$SDLMIXERDIR` used in
building `SDL_mixer`.
- `SDLGFXDIR` is the path to the cross-compiled version for `SDL_gfx`,
which would correspond to `./configure --prefix=$SDLGFXDIR` used in
building `SDL_gfx`.
- `ENETDIR` is the path to the cross-compiled version for `enet`,
which would correspond to `./configure --prefix=$ENETDIR` used in
building `enet`.

A stripped version of the executable, as well as game assets, will be
located in `build/cdimg`.

#### Dependencies

A cross-compiled `i386-mingw32` version of all required dependencies
is needed before building `rts`.

Note upstream CMake does not provide `FindSDL_gfx.cmake` as of the time
of this writing, so it is provisionally provided on
[this repository](cmake/FindSDL_gfx.cmake).

[Ongoing upstream PR](https://gitlab.kitware.com/cmake/cmake/-/merge_requests/7475)

Read [the documentation](doc/BUILD-win9x.md) for further reference on
how to build the dependencies from source.

## License

Unless stated otherwise, **RTS** follows the license described by the
`LICENSE` file, which currently is the GNU General Public License v3
or later. Original versions of other works under various licenses are
also distributed in this project, that are located inside the
`res/orig` directory. This directory also contains a `LICENSE`
describing the source and license of each individual file.

Derivative works have been also created from these files for this
project, that are located inside the `res` directory. A `LICENSE`
file is also provided to describe the relationship between the
original and derived works.

## Copyright notice

Microsoft®, Linux®, Sony® and PlayStation® are registered trademarks of
their respective owners.
