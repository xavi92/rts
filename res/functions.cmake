function(sprite)
    set(options "")
    set(multiValueArgs "")
    set(oneValueArgs NAME X Y TRANSPARENT BPP CX CY)
    cmake_parse_arguments(SPRITE "${options}" "${oneValueArgs}"
        "${multiValueArgs}" ${ARGN})

    if(PS1_BUILD)
        if(${SPRITE_TRANSPARENT})
            set(trans -mpink)
        endif()

        if(NOT "${SPRITE_BPP}" STREQUAL "16")
            set(clut "-clut=${SPRITE_CX},${SPRITE_CY}")
        endif()

        add_custom_target(${SPRITE_NAME}_img ALL
            bmp2tim ${SPRITE_NAME}.bmp ${CMAKE_CURRENT_BINARY_DIR}/${SPRITE_NAME}
                ${SPRITE_BPP} -org=${SPRITE_X},${SPRITE_Y} ${clut} ${trans}
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            DEPENDS ${SPRITE_NAME}.bmp
            BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/${SPRITE_NAME})
        add_dependencies(iso ${SPRITE_NAME}_img)
    elseif(SDL1_2_BUILD)
        if(${SPRITE_TRANSPARENT})
            set(trans "transparent=1")
        else()
            set(trans "transparent=0")
        endif()

        add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${SPRITE_NAME}
            COMMAND ${TOOLS_PREFIX}/bin/add-header ${trans} ${SPRITE_NAME}_24.bmp ${CMAKE_CURRENT_BINARY_DIR}/${SPRITE_NAME}
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            DEPENDS ${SPRITE_NAME}_24.bmp
            VERBATIM)
        add_custom_target(${SPRITE_NAME}_img
            DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${SPRITE_NAME})
        add_dependencies(${SPRITE_NAME}_img tools)
    endif()
endfunction()

function(sound)
    set(options "")
    set(multiValueArgs "")
    set(oneValueArgs NAME LOOP)
    cmake_parse_arguments(SOUND "${options}" "${oneValueArgs}"
        "${multiValueArgs}" ${ARGN})

    if(PS1_BUILD)
        if(${SOUND_LOOP})
            set(loop -L)
        endif()

        add_custom_target(${SOUND_NAME}_snd ALL
            wav2vag ${SOUND_NAME}.wav
                ${CMAKE_CURRENT_BINARY_DIR}/${SOUND_NAME} ${loop}
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            DEPENDS ${SOUND_NAME}.wav
            BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/${SOUND_NAME})
        add_dependencies(iso ${SOUND_NAME}_snd)
    elseif(SDL1_2_BUILD)
        if(${SOUND_LOOP})
            set(loop "loop=1")
        else()
            set(loop "loop=0")
        endif()

        # Reference: https://gist.github.com/socantre/7ee63133a0a3a08f3990
        add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${SOUND_NAME}
            COMMAND ${TOOLS_PREFIX}/bin/add-header ${loop} ${SOUND_NAME}.wav ${CMAKE_CURRENT_BINARY_DIR}/${SOUND_NAME}
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            DEPENDS ${SOUND_NAME}.wav
            VERBATIM)
        add_custom_target(${SOUND_NAME}_snd
            DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${SOUND_NAME})
        add_dependencies(${SOUND_NAME}_snd tools)
    endif()
endfunction()

function(container)
    set(options "")
    set(multiValueArgs SPRITES SOUNDS)
    set(oneValueArgs NAME)
    cmake_parse_arguments(CONTAINER "${options}" "${oneValueArgs}"
        "${multiValueArgs}" ${ARGN})

    add_custom_command(OUTPUT ${cdroot}/${CONTAINER_NAME}.cnt
        COMMAND ${TOOLS_PREFIX}/bin/container ${CONTAINER_SPRITES} ${CONTAINER_SOUNDS}
            ${cdroot}/${CONTAINER_NAME}.cnt
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        VERBATIM)

    add_custom_target(${CONTAINER_NAME}_container
        DEPENDS ${cdroot}/${CONTAINER_NAME}.cnt)
    add_dependencies(${PROJECT_NAME} ${CONTAINER_NAME}_container)
    add_dependencies(${CONTAINER_NAME}_container tools)

    foreach(sprite ${CONTAINER_SPRITES})
        add_dependencies(${CONTAINER_NAME}_container ${sprite}_img)
    endforeach()

    foreach(sound ${CONTAINER_SOUNDS})
        add_dependencies(${CONTAINER_NAME}_container ${sound}_snd)
    endforeach()

    if(PS1_BUILD)
        add_dependencies(iso ${CONTAINER_NAME}_container)
    endif()
endfunction()
