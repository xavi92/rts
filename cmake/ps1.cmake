find_package(PSXSDK 0.7.1 REQUIRED)

target_compile_definitions(${PROJECT_NAME} PUBLIC FIXMATH_FAST_SIN PSXSDK_DEBUG)
add_custom_target(exe ALL elf2exe ${PROJECT_NAME}
    ${cdroot}/${PROJECT_NAME}.exe -mark="A homebrew game created with PSXSDK"
    DEPENDS ${PROJECT_NAME})
add_custom_target(iso ALL mkisofs -o ${PROJECT_NAME}.iso -V ${PROJECT_NAME}
    -sysid PLAYSTATION ${cdroot} DEPENDS exe)
set(license $ENV{PSXSDK_PATH}/share/licenses/infoeur.dat)
add_custom_target(bin_cue ALL mkpsxiso ${PROJECT_NAME}.iso ${PROJECT_NAME}.bin
    ${license} -s DEPENDS iso)

if(NOT EXISTS "${cdroot}/system.cnf")
    file(COPY "src/system.cnf" DESTINATION "${cdroot}")
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    set(cflags ${cflags} -Og)
else()
    set(cflags ${cflags} -Os)
endif()

set(cflags ${cflags} -fshort-enums)
# libfixmath is already bundled with PSXSDK.
