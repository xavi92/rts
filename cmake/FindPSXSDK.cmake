# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#[=======================================================================[.rst:
FindPSXSDK
-----------

Find PSXSDK (an open source software development kit for the original
Sony PlayStation) libraries and header files.

Imported targets
^^^^^^^^^^^^^^^^

This module defines the following :prop_tgt:`IMPORTED` targets:

  ``PSXSDK::PSXSDK``,
  The PSXSDK `psx` library, if found.

Result variables
^^^^^^^^^^^^^^^^

This module will set the following variables in your project:

``PSXSDK_FOUND``
  true if PSXSDK libraries and header files were found.
``PSXSDK_VERSION``
  PSXSDK release version
``PSXSDK_INCLUDE_DIRS``
  the directory containing the PSXSDK headers; note
  ``PSXSDK_INCLUDE_DIRS`` is also required
``PSXSDK_LIBRARIES``
  PSXSDK libraries to be linked; note ``PSXSDK_LIBRARIES`` is also
  required

#]=======================================================================]

find_path(PSXSDK_INCLUDE_DIRS
    NAMES
        huff.h
        meidogte.h
        meidogte_inline.h
        memcard.h
        modplay.h
        psxbios.h
        psxcdrom.h
        psxgpu.h
        psxgte.h
        psx.h
        psxpad.h
        psxsio.h
        psxspu.h
        psxutil.h
        runexe.h
        search.h
    HINTS
        ENV PSXSDK_PATH
    PATH_SUFFIXES
        include
)

find_library(PSXSDK_LIBRARIES
    NAMES psx
    HINTS
        ENV PSXSDK_PATH
    PATH_SUFFIXES
        lib
)

if(PSXSDK_INCLUDE_DIRS AND EXISTS "${PSXSDK_INCLUDE_DIRS}/psx.h")
    set(version_regex "^#define[ \t]+PSXSDK_VERSION_STRING[ \t]+\"([0-9\.]+)\"$")
    file(STRINGS "${PSXSDK_INCLUDE_DIRS}/psx.h" PSXSDK_VERSION_LINE REGEX ${version_regex})
    string(REGEX REPLACE ${version_regex} "\\1" PSXSDK_VERSION "${PSXSDK_VERSION_LINE}")
    unset(PSXSDK_VERSION_LINE)
    unset(version_regex)
endif()

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(PSXSDK
    REQUIRED_VARS
        PSXSDK_LIBRARIES PSXSDK_INCLUDE_DIRS
    VERSION_VAR
        PSXSDK_VERSION
)

if(PSXSDK_FOUND)
    if(NOT TARGET PSXSDK::PSXSDK)
        add_library(PSXSDK::PSXSDK INTERFACE IMPORTED)
        target_include_directories(PSXSDK::PSXSDK
            INTERFACE "${PSXSDK_INCLUDE_DIRS}")
        set_target_properties(PSXSDK::PSXSDK PROPERTIES
            IMPORTED_LOCATION "${PSXSDK_LIBRARIES}")
    endif()
endif()
