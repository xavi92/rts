if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        set(cflags ${cflags} -Og)
else()
    set(cflags ${cflags} -O3)
endif()

include("${CMAKE_CURRENT_LIST_DIR}/fetch-libfixmath.cmake")
add_subdirectory(libfixmath)
