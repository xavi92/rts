add_custom_command(OUTPUT ${cdroot}/${PROJECT_NAME}
        COMMAND i386-mingw32-strip ${PROJECT_NAME} -o ${cdroot}/${PROJECT_NAME}
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        DEPENDS ${PROJECT_NAME}
        VERBATIM)
add_custom_target(stripped-exe ALL DEPENDS ${cdroot}/${PROJECT_NAME})

target_link_libraries(SDL::SDL INTERFACE gdi32 user32 winmm dxguid)
target_link_libraries(ENET INTERFACE ws2_32)

add_compile_options(-march=i386)

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    # i386-mingw32-gcc 3.4.5 does not support -Og.
    set(cflags ${cflags} -O0)
else()
    set(cflags ${cflags} -O2)
endif()

include("${CMAKE_CURRENT_LIST_DIR}/fetch-libfixmath.cmake")
add_subdirectory(libfixmath)
