find_path(libfixmath "CMakeLists.txt"
    PATHS "${PROJECT_SOURCE_DIR}/libfixmath/"
    NO_DEFAULT_PATH)

if(${libfixmath} STREQUAL "libfixmath-NOTFOUND")
    message(STATUS "Fetching submodules...")
    execute_process(COMMAND git submodule update --init --recursive)
endif()
